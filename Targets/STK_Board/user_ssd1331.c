
#include "user_ssd1331.h"
#include <string.h>

#pragma region cSSD1331Worker
typedef enum {
    NO_Error_cSSD1331Worker = 0,
    SEMAPHORE_Error_cSSD1331Worker,
    HAL_SPI_Error_cSSD1331Worker,
    SPI_COM_TIMEOUT_Error_cSSD1331Worker,
    NUM_ERRORS_cSSD1331Worker
} Error_cSSD1331Worker;

typedef enum StateMachine_cSSD1331Worker_t {
    NO_StateMachine_cSSD1331Worker,
    READY_StateMachine_cSSD1331Worker,
    ERROR_StateMachine_cSSD1331Worker,
    NUM_StateMachine_cSSD1331Worker
} StateMachine_cSSD1331Worker;

/**
 * @brief a worker to handle display
 *
 * @todo
 *  - add buffer for process comMunication btw cSSD1331Worker and other tasks
 *
 */
typedef struct cSSD1331Worker_t {
    int id;
    Error_cSSD1331Worker error_code;
    StateMachine_cSSD1331Worker state;

    SPI_HandleTypeDef *hspi;
    GFX_Object *gfx;

    uint8_t rxSpiBuffer[1];
    uint8_t txSpiBuffer[1];
    char spiBusyFlag;
} cSSD1331Worker;

void HandleTxRxCpltCallback_cSSD1331Worker(cSSD1331Worker *me) {
    me->spiBusyFlag = 0;
}
int WriteSPI_cSSD1331Worker(cSSD1331Worker *me, uint8_t c) {
    int ret = 0;
    // todo using blocking for now
    while (me->spiBusyFlag == 1) {
        osDelay(1);
    }
    me->txSpiBuffer[0] = c;
    me->spiBusyFlag = 1;
    int ret1 = HAL_SPI_TransmitReceive_DMA(me->hspi, me->txSpiBuffer,
                                           me->rxSpiBuffer, 1);
    if (ret1 != HAL_OK) {
        ret = HAL_SPI_Error_cSSD1331Worker;
        me->state = ERROR_StateMachine_cSSD1331Worker;
    }
    return ret;
}
void Init_cSSD1331Worker(cSSD1331Worker *me) {
    memset(me, 0, sizeof(cSSD1331Worker));
}
void Setup_cSSD1331Worker(cSSD1331Worker *me, SPI_HandleTypeDef *hspi,
                          GFX_Object *gfx) { //
    me->hspi = hspi;
    me->gfx = gfx;

    SSD1331_init();
    Adafruit_GFX_Init(me->gfx);
    Adafruit_GFX_setTextColorAndBackground(me->gfx, WHITE, BLACK);
    Adafruit_GFX_fillScreen(me->gfx, BLACK);
}
#pragma endregion

static cSSD1331Worker userSSD1331;

#pragma region SSD1331_IO
void WritePin_DC_user_ssd1331(int flag) {
    if (flag) {
        HAL_GPIO_WritePin(GPIO_D_C_LCD_GPIO_Port, GPIO_D_C_LCD_Pin,
                          GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(GPIO_D_C_LCD_GPIO_Port, GPIO_D_C_LCD_Pin,
                          GPIO_PIN_RESET);
    }
}
void WritePin_CS_user_ssd1331(int flag) {
    if (flag) {
        HAL_GPIO_WritePin(GPIO_CS_LCD_GPIO_Port, GPIO_CS_LCD_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(GPIO_CS_LCD_GPIO_Port, GPIO_CS_LCD_Pin,
                          GPIO_PIN_RESET);
    }
}
void WritePin_RST_user_ssd1331(int flag) {
    if (flag) {
        HAL_GPIO_WritePin(GPIO_RST_LCD_GPIO_Port, GPIO_RST_LCD_Pin,
                          GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(GPIO_RST_LCD_GPIO_Port, GPIO_RST_LCD_Pin,
                          GPIO_PIN_RESET);
    }
}
void WriteSPI_user_ssd1331(uint8_t c) {
    WriteSPI_cSSD1331Worker(&userSSD1331, c);
}
void HAL_SPI_TxRxCpltCallback_user_ssd1331(SPI_HandleTypeDef *hspi) {
    if (userSSD1331.hspi == hspi) {
        HandleTxRxCpltCallback_cSSD1331Worker(&userSSD1331);
    }
}
#pragma endregion

void Setup_user_ssd1331(SPI_HandleTypeDef *hspi, GFX_Object *gfx) {
    cSSD1331Worker *me = &userSSD1331;
    Init_cSSD1331Worker(me);
    Setup_cSSD1331Worker(me, hspi, gfx);
}
