#ifndef _USER_SSD1331_H
#define _USER_SSD1331_H
#ifdef __cplusplus
extern "C" {
#endif

#include "Adafruit_GFX.h"
#include "SSD1331_2.h"
#include "main.h"

typedef enum {
    NO_Error_cSSD1331Worker = 0,
    SEMAPHORE_Error_cSSD1331Worker,
    HAL_SPI_Error_cSSD1331Worker,
    SPI_COM_TIMEOUT_Error_cSSD1331Worker,
    NUM_ERRORS_cSSD1331Worker
} Error_cSSD1331Worker;

typedef enum StateMachine_cSSD1331Worker_t {
    NO_StateMachine_cSSD1331Worker,
    READY_StateMachine_cSSD1331Worker,
    ERROR_StateMachine_cSSD1331Worker,
    NUM_StateMachine_cSSD1331Worker
} StateMachine_cSSD1331Worker;

/**
 * @brief a worker to handle display
 *
 * @todo
 *  - add buffer for process comMunication btw cSSD1331Worker and other tasks
 *
 */
typedef struct cSSD1331Worker_t {
    int id;
    Error_cSSD1331Worker error_code;
    StateMachine_cSSD1331Worker state;

    SPI_HandleTypeDef *hspi;
    GFX_Object *gfx;

    uint8_t rxSpiBuffer[1];
    uint8_t txSpiBuffer[1];
    char spiBusyFlag;
} cSSD1331Worker;

/**
 * @brief callback when spi com is compileted
 *
 * @param me
 */
void HandleTxRxCpltCallback_cSSD1331Worker(cSSD1331Worker *me);
/**
 * @brief send a char to spi port
 *
 * @param me
 * @param c
 * @return int @Error_cSSD1331Worker
 */
int WriteSPI_cSSD1331Worker(cSSD1331Worker *me, uint8_t c);
void Init_cSSD1331Worker(cSSD1331Worker *me);
void Setup_cSSD1331Worker(cSSD1331Worker *me, SPI_HandleTypeDef *hspi,
                          GFX_Object *gfx);

/**
 * @brief set dc pin to high or low
 *
 * @param flag =0 to set pin low; >0 to set pin high
 */
void WritePin_DC_user_ssd1331(int flag);
void WritePin_CS_user_ssd1331(int flag);
void WritePin_RST_user_ssd1331(int flag);
/**
 * @brief send a char to spi port
 *
 * @param c
 */
void WriteSPI_user_ssd1331(uint8_t c);
/**
 * @brief callback when spi com is compileted
 *
 * @param hspi
 */
void HAL_SPI_TxRxCpltCallback_user_ssd1331(SPI_HandleTypeDef *hspi);
/**
 * @brief help to setup gfx with spi port
 *
 * @param hspi
 * @param gfx
 */
void Setup_user_ssd1331(SPI_HandleTypeDef *hspi, GFX_Object *gfx);

#ifdef __cplusplus
}
#endif
#endif
