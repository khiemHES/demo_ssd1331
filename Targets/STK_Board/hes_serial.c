#include "hes_serial.h"
#include "cSerialHAL.h"
#include "main.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#define THREAD_STACK_SIZE_Task_hes_serial__ (128)
#define PERIOD_UPDATE_hes_serial (5)
#define SIZE_TX_BUFFER_hes_serial (256)
#define SIZE_RX_BUFFER_hes_serial (8)

#pragma region INTERFACE_cSerialWorker
typedef enum FSM_cSerialWorker_t
{
    NO_STATE_FSM_cSerialWorker = 0,
    READY_FSM_cSerialWorker,
    NUM_FSM_cSerialWorker
} FSM_cSerialWorker;
typedef struct cSerialWorker_t
{
    int id;
    uint32_t error_code;
    FSM_cSerialWorker state;
    cSerialHAL *serial_port;
    osThreadId_t taskID;
} cSerialWorker;

void Init_cSerialWorker(cSerialWorker *me)
{
    memset(me, 0, sizeof(cSerialWorker));
}
int Setup_cSerialWorker(cSerialWorker *me, cSerialHAL *serialPort)
{
    me->serial_port = serialPort;
    return 0;
}
void DeInit_cSerialWorker(cSerialWorker *me)
{
    me->state = NO_STATE_FSM_cSerialWorker;
    if (me->taskID)
    {
        osThreadTerminate(me->taskID);
    }
}
#pragma endregion

extern UART_HandleTypeDef huart1;
cSerialHAL debug_serial;
static cSerialWorker hesSerial;

#pragma region PRIVATE_INTERFACE_hes_serial
/**
 * @brief just wrapper around singleton objs
 *
 * @param txStr
 * @param len
 * @return int
 */
static inline int PutBuffer__(uint8_t *txStr, uint32_t len)
{
    return PutBuffer_cSerialHAL(&debug_serial, txStr, len);
}

static void Task_hes_serial__(void *argument)
{
    while (1)
    {
        if (hesSerial.state)
        {
            osDelay(PERIOD_UPDATE_hes_serial);
            Update_cSerialHAL(&debug_serial);
        }
    }
}
#pragma endregion

#pragma region PUBLIC_INTERFACE_hes_serial

/// callbacks for the mcu interrupt
void HAL_UART_TxCpltCallback_hes_serial(void)
{
    HandleTxCpltCallback_cSerialHAL(&debug_serial);
}
void HAL_UART_RxCpltCallback_hes_serial(void)
{
    HandleRxCpltCallback_cSerialHAL(&debug_serial);
}
/**
 * @brief provide printf-kind interface
 *
 * @param format
 * @param ...
 * @return int
 */
int Printf_hes_serial(char *format, ...)
{
    int ret = 0;
    char msg[TX_BUFFER_SIZE_DEBUG_SERIAL];

    va_list args;
    va_start(args, format);
    int strLength = vsnprintf(msg, TX_BUFFER_SIZE_DEBUG_SERIAL, format, args);
    if (strLength < TX_BUFFER_SIZE_DEBUG_SERIAL && strLength >= 0)
    {
        PutBuffer__((uint8_t *)msg, strLength);
    }
    else
    {
        ret = 1;
    }
    va_end(args);
    return ret;
}
/**
 * @brief check if the serial intance need to send or read anything
 *
 * @return int
 */
int IsIdle_hes_serial(void)
{
    return !IsSendable_cSerialHAL(&debug_serial) &&
           !IsReadable_cSerialHAL(&debug_serial);
}
/**
 * @brief check if receiving any chars
 *
 * @return int
 */
int IsReadable_hes_serial(void) { return IsReadable_cSerialHAL(&debug_serial); }
/**
 * @brief read one char from receiving_serial_buffer
 *
 * @param retCharPtr
 * @return int
 */
int ReadChar_hes_serial(char *retCharPtr)
{
    cSerialHAL *me = &debug_serial;
    osStatus_t ret1 =
        osMessageQueueGet(me->rxQueue, retCharPtr, NULL, me->rxBlockTime);
    return (int)ret1;
}
void DeInit_hes_serial(void)
{
    DeInit_cSerialHAL(&debug_serial);
    DeInit_cSerialWorker(&hesSerial);
}
void Setup_hes_serial()
{
    Init_cSerialWorker(&hesSerial);
    Setup_cSerialWorker(&hesSerial, &debug_serial);

    Init_cSerialHAL(&debug_serial);
    debug_serial.id = 1;
    Setup_cSerialHAL(&debug_serial, &huart1, SIZE_TX_BUFFER_hes_serial,
                     SIZE_RX_BUFFER_hes_serial);

    hesSerial.state = READY_FSM_cSerialWorker;
    osThreadAttr_t threadAttr = {.name = "Task_hes_serial__ ",
                                 .stack_size =
                                     THREAD_STACK_SIZE_Task_hes_serial__,
                                 .priority = osPriorityLow};
    hesSerial.taskID = osThreadNew(Task_hes_serial__, NULL, &threadAttr);
}
#pragma endregion
