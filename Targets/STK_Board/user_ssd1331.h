#ifndef _USER_SSD1331_H
#define _USER_SSD1331_H
#ifdef __cplusplus
extern "C" {
#endif

#include "Adafruit_GFX.h"
#include "SSD1331_2.h"
#include "main.h"

/**
 * @brief set dc pin to high or low
 *
 * @param flag =0 to set pin low; >0 to set pin high
 */
void WritePin_DC_user_ssd1331(int flag);
void WritePin_CS_user_ssd1331(int flag);
void WritePin_RST_user_ssd1331(int flag);
/**
 * @brief send a char to spi port
 *
 * @param c
 */
void WriteSPI_user_ssd1331(uint8_t c);
/**
 * @brief callback when spi com is compileted
 *
 * @param hspi
 */
void HAL_SPI_TxRxCpltCallback_user_ssd1331(SPI_HandleTypeDef *hspi);
/**
 * @brief help to setup gfx with spi port
 *
 * @param hspi
 * @param gfx
 */
void Setup_user_ssd1331(SPI_HandleTypeDef *hspi, GFX_Object *gfx);

#ifdef __cplusplus
}
#endif
#endif
