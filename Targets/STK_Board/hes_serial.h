#ifndef _HES_SERIALHAL_H
#define _HES_SERIALHAL_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

void DeInit_hes_serial(void);
void Setup_hes_serial(void);
void HAL_UART_TxCpltCallback_hes_serial(void);
void HAL_UART_RxCpltCallback_hes_serial(void);
int PutBuffer_hes_serial(uint8_t *txStr, uint32_t len);
int Printf_hes_serial(char *format, ...);
int IsReadable_hes_serial(void);
int ReadChar_hes_serial(char *retCharPtr);
int IsIdle_hes_serial(void);

#ifdef __cplusplus
}
#endif
#endif
