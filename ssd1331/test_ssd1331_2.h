#ifndef _TEST_SSD1331_H
#define _TEST_SSD1331_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

int Setup_test_ssd1331(void);
int Test_simple_ssd1331(void);

#ifdef __cplusplus
}
#endif
#endif
