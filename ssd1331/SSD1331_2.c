/***************************************************
  This is a library for the 0.96" 16-bit Color OLED with SSD1331 driver chip

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/684

  These displays use SPI to communicate, 4 or 5 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "SSD1331_2.h"
#include "Adafruit_GFX.h"
#include "glcdfont.c"

/********************************** low level pin interface */

extern void WritePin_DC_user_ssd1331(int flag);
extern void WritePin_CS_user_ssd1331(int flag);
extern void WritePin_RST_user_ssd1331(int flag);
extern void WriteSPI_user_ssd1331(uint8_t c);

void writeCommand(uint8_t c) {
    WritePin_DC_user_ssd1331(0);
    WritePin_CS_user_ssd1331(0);
    WriteSPI_user_ssd1331(c);
    WritePin_CS_user_ssd1331(1);
}

void writeData(uint8_t d) {
    WritePin_DC_user_ssd1331(1);
    WritePin_CS_user_ssd1331(0);
    WriteSPI_user_ssd1331(d);
    WritePin_CS_user_ssd1331(1);
}

/***********************************/

void goHome(void) { goTo(0, 0); }

void goTo(int x, int y) {
    if ((x >= TFTWIDTH) || (y >= TFTHEIGHT))
        return;

    // set x and y coordinate
    writeCommand(SSD1331_CMD_SETCOLUMN);
    writeCommand(x);
    writeCommand(TFTWIDTH - 1);

    writeCommand(SSD1331_CMD_SETROW);
    writeCommand(y);
    writeCommand(TFTHEIGHT - 1);
}

uint16_t Color565(uint8_t r, uint8_t g, uint8_t b) {
    uint16_t c;
    c = r >> 3;
    c <<= 6;
    c |= g >> 2;
    c <<= 5;
    c |= b >> 3;

    return c;
}

void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) {
    // check rotation, move pixel around if necessary
    /* switch (getRotation()) {
    case 1:
      gfx_swap(x0, y0);
      gfx_swap(x1, y1);
      x0 = TFTWIDTH - x0 - 1;
      x1 = TFTWIDTH - x1 - 1;
      break;
    case 2:
      x0 = TFTWIDTH - x0 - 1;
      y0 = TFTHEIGHT - y0 - 1;
      x1 = TFTWIDTH - x1 - 1;
      y1 = TFTHEIGHT - y1 - 1;
      break;
    case 3:
      gfx_swap(x0, y0);
      gfx_swap(x1, y1);
      y0 = TFTHEIGHT - y0 - 1;
      y1 = TFTHEIGHT - y1 - 1;
      break;
    }
  */
    // Boundary check
    if ((y0 >= TFTHEIGHT) && (y1 >= TFTHEIGHT))
        return;
    if ((x0 >= TFTWIDTH) && (x1 >= TFTWIDTH))
        return;
    if (x0 >= TFTWIDTH)
        x0 = TFTWIDTH - 1;
    if (y0 >= TFTHEIGHT)
        y0 = TFTHEIGHT - 1;
    if (x1 >= TFTWIDTH)
        x1 = TFTWIDTH - 1;
    if (y1 >= TFTHEIGHT)
        y1 = TFTHEIGHT - 1;

    writeCommand(SSD1331_CMD_DRAWLINE);
    writeCommand(x0);
    writeCommand(y0);
    writeCommand(x1);
    writeCommand(y1);
    // delay(SSD1331_DELAYS_HWLINE);
    // vTaskDelay( 1 / portTICK_RATE_MS );
    writeCommand((uint8_t)((color >> 11) << 1));
    writeCommand((uint8_t)((color >> 5) & 0x3F));
    writeCommand((uint8_t)((color << 1) & 0x3F));
    // delay(SSD1331_DELAYS_HWLINE);
    // vTaskDelay( 1 / portTICK_RATE_MS );
}

void drawPixel(int32_t x, int32_t y, uint16_t color) {
    // if ((x < 0) || (x >= width()) || (y < 0) || (y >= height())) return;
    if ((x < 0) || (x >= TFTWIDTH) || (y < 0) || (y >= TFTHEIGHT))
        return;

    // check rotation, move pixel around if necessary
    /*  switch (getRotation()) {
    case 1:
      gfx_swap(x, y);
      x = TFTWIDTH - x - 1;
      break;
    case 2:
      x = TFTWIDTH - x - 1;
      y = TFTHEIGHT - y - 1;
      break;
    case 3:
      gfx_swap(x, y);
      y = TFTHEIGHT - y - 1;
      break;
    }
  */
    goTo(x, y);

    // setup for data
    // *rsportreg |= rspin;
    // *csportreg &= ~ cspin;
    WritePin_DC_user_ssd1331(1);
    // digitalWrite(_rs, HIGH);

    WritePin_CS_user_ssd1331(0);
    WriteSPI_user_ssd1331(color >> 8);
    WriteSPI_user_ssd1331(color);
    WritePin_CS_user_ssd1331(1);

    // *csportreg |= cspin;
}

void pushColor(uint16_t color) {
    // setup for data
    // *rsportreg |= rspin;
    // *csportreg &= ~ cspin;

    WriteSPI_user_ssd1331(color >> 8);
    WriteSPI_user_ssd1331(color);

    // *csportreg |= cspin;
}

void SSD1331_init(void) {
    WritePin_RST_user_ssd1331(1);
    osDelay(1);
    WritePin_RST_user_ssd1331(0);
    osDelay(10);
    WritePin_RST_user_ssd1331(1);

    // Initialization Sequence
    writeCommand(SSD1331_CMD_DISPLAYOFF); // 0xAE
    writeCommand(SSD1331_CMD_SETREMAP);   // 0xA0
#if defined SSD1331_COLORORDER_RGB
    writeCommand(0x72); // RGB Color
#else
    writeCommand(0x76); // BGR Color
#endif
    writeCommand(SSD1331_CMD_STARTLINE); // 0xA1
    writeCommand(0x0);
    writeCommand(SSD1331_CMD_DISPLAYOFFSET); // 0xA2
    writeCommand(0x0);
    writeCommand(SSD1331_CMD_NORMALDISPLAY); // 0xA4
    writeCommand(SSD1331_CMD_SETMULTIPLEX);  // 0xA8
    writeCommand(0x3F);                      // 0x3F 1/64 duty
    writeCommand(SSD1331_CMD_SETMASTER);     // 0xAD
    writeCommand(0x8E);
    writeCommand(SSD1331_CMD_POWERMODE); // 0xB0
    writeCommand(0x0B);
    writeCommand(SSD1331_CMD_PRECHARGE); // 0xB1
    writeCommand(0x31);
    writeCommand(SSD1331_CMD_CLOCKDIV); // 0xB3
    writeCommand(0xF0); // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio
                        // (A[3:0]+1 = 1..16)
    writeCommand(SSD1331_CMD_PRECHARGEA); // 0x8A
    writeCommand(0x64);
    writeCommand(SSD1331_CMD_PRECHARGEB); // 0x8B
    writeCommand(0x78);
    writeCommand(SSD1331_CMD_PRECHARGEA); // 0x8C
    writeCommand(0x64);
    writeCommand(SSD1331_CMD_PRECHARGELEVEL); // 0xBB
    writeCommand(0x3A);
    writeCommand(SSD1331_CMD_VCOMH); // 0xBE
    writeCommand(0x3E);
    writeCommand(SSD1331_CMD_MASTERCURRENT); // 0x87
    writeCommand(0x06);
    writeCommand(SSD1331_CMD_CONTRASTA); // 0x81
    writeCommand(0x91);
    writeCommand(SSD1331_CMD_CONTRASTB); // 0x82
    writeCommand(0x50);
    writeCommand(SSD1331_CMD_CONTRASTC); // 0x83
    writeCommand(0x7D);
    writeCommand(SSD1331_CMD_DISPLAYON); //--turn on oled panel
}

/********************************* low level pin initialization */
