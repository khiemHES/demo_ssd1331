#include "test_ssd1331_2.h"
#include "Adafruit_GFX.h"
#include "SSD1331_2.h"
#include "user_ssd1331.h"

GFX_Object testedScreen;
extern SPI_HandleTypeDef hspi3;

int Setup_test_ssd1331(void) {
    Setup_user_ssd1331(&hspi3, &testedScreen);
    return 0;
}
int Test_simple_ssd1331(void) {
    char mystring[128] = {0};
    float values[4];

    while (1) {
        osDelay(1000);
        sprintf(mystring, "Hello !!!!");
        Adafruit_GFX_setCursor(&testedScreen, 0, 20);
        Adafruit_GFX_setTextSize(&testedScreen, 2);
        Adafruit_GFX_println(&testedScreen, mystring);
    }
}
