/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "project_config.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define HEATER_LS_CC_hes_pwm 0x0004
#define STACK_FAN_CC_hes_pwm 0x0000
#define HSG_PUMP_CC_hes_pump 4-1
#define ADC_PT1K_TEMP_1_Pin GPIO_PIN_0
#define ADC_PT1K_TEMP_1_GPIO_Port GPIOC
#define ADC_PT1K_TEMP_2_Pin GPIO_PIN_1
#define ADC_PT1K_TEMP_2_GPIO_Port GPIOC
#define ADC_PRESSURE_PRI_Pin GPIO_PIN_2
#define ADC_PRESSURE_PRI_GPIO_Port GPIOC
#define ADC_PRESSURE_SEC_Pin GPIO_PIN_3
#define ADC_PRESSURE_SEC_GPIO_Port GPIOC
#define ADC_BATT_TEMP_Pin GPIO_PIN_2
#define ADC_BATT_TEMP_GPIO_Port GPIOF
#define ADC_BATT_CUR_Pin GPIO_PIN_0
#define ADC_BATT_CUR_GPIO_Port GPIOA
#define ADC_BATT_VOL_Pin GPIO_PIN_1
#define ADC_BATT_VOL_GPIO_Port GPIOA
#define ADC_FUELCELL_VOL_Pin GPIO_PIN_2
#define ADC_FUELCELL_VOL_GPIO_Port GPIOA
#define ADC_FUELCELL_CUR_Pin GPIO_PIN_3
#define ADC_FUELCELL_CUR_GPIO_Port GPIOA
#define SPI1_CS_ADS1118_Pin GPIO_PIN_4
#define SPI1_CS_ADS1118_GPIO_Port GPIOA
#define SPI1_SCK_ADS1118_Pin GPIO_PIN_5
#define SPI1_SCK_ADS1118_GPIO_Port GPIOA
#define SPI1_MISO_ADS1118_Pin GPIO_PIN_6
#define SPI1_MISO_ADS1118_GPIO_Port GPIOA
#define SPI1_MOSI_ADS1118_Pin GPIO_PIN_7
#define SPI1_MOSI_ADS1118_GPIO_Port GPIOA
#define GPIO_FC_SHORT_Pin GPIO_PIN_4
#define GPIO_FC_SHORT_GPIO_Port GPIOC
#define GPIO_FC_LOAD_Pin GPIO_PIN_5
#define GPIO_FC_LOAD_GPIO_Port GPIOC
#define GPIO_EFUSE_nFLT_Pin GPIO_PIN_0
#define GPIO_EFUSE_nFLT_GPIO_Port GPIOB
#define GPIO_EFUSE_EN_Pin GPIO_PIN_1
#define GPIO_EFUSE_EN_GPIO_Port GPIOB
#define GPIO_PU_VA_CTRL_Pin GPIO_PIN_7
#define GPIO_PU_VA_CTRL_GPIO_Port GPIOE
#define GPIO_SU_VA_CTR_Pin GPIO_PIN_8
#define GPIO_SU_VA_CTR_GPIO_Port GPIOE
#define PWM_STACKFAN_CTRL_Pin GPIO_PIN_9
#define PWM_STACKFAN_CTRL_GPIO_Port GPIOE
#define GPIO_CHARGER_EN_Pin GPIO_PIN_10
#define GPIO_CHARGER_EN_GPIO_Port GPIOE
#define PWM_HEATER_LS_CTRL_Pin GPIO_PIN_11
#define PWM_HEATER_LS_CTRL_GPIO_Port GPIOE
#define GPIO_HEATER_HS_CTRL_Pin GPIO_PIN_12
#define GPIO_HEATER_HS_CTRL_GPIO_Port GPIOE
#define PWM_REACTFAN_CTRL_Pin GPIO_PIN_13
#define PWM_REACTFAN_CTRL_GPIO_Port GPIOE
#define PWM_WA_PUMP_CTRL_Pin GPIO_PIN_14
#define PWM_WA_PUMP_CTRL_GPIO_Port GPIOE
#define GPIO_WA_VA_CTRL_Pin GPIO_PIN_15
#define GPIO_WA_VA_CTRL_GPIO_Port GPIOE
#define SPI2_CS_SD_Pin GPIO_PIN_12
#define SPI2_CS_SD_GPIO_Port GPIOB
#define SPI2_SCK_SD_Pin GPIO_PIN_13
#define SPI2_SCK_SD_GPIO_Port GPIOB
#define SPI2_MISO_SD_Pin GPIO_PIN_14
#define SPI2_MISO_SD_GPIO_Port GPIOB
#define SPI2_MOSI_SD_Pin GPIO_PIN_15
#define SPI2_MOSI_SD_GPIO_Port GPIOB
#define GPIO_CD_SD_Pin GPIO_PIN_10
#define GPIO_CD_SD_GPIO_Port GPIOD
#define GPIO_PWR_KILL_Pin GPIO_PIN_11
#define GPIO_PWR_KILL_GPIO_Port GPIOA
#define GPIO_KEY_PRESS_Pin GPIO_PIN_12
#define GPIO_KEY_PRESS_GPIO_Port GPIOA
#define GPIO_DEBUG_LED_Pin GPIO_PIN_6
#define GPIO_DEBUG_LED_GPIO_Port GPIOF
#define SPI3_SCK_LCD_Pin GPIO_PIN_10
#define SPI3_SCK_LCD_GPIO_Port GPIOC
#define SPI3_MISO_LCD_Pin GPIO_PIN_11
#define SPI3_MISO_LCD_GPIO_Port GPIOC
#define SPI3_MOSI_LCD_Pin GPIO_PIN_12
#define SPI3_MOSI_LCD_GPIO_Port GPIOC
#define GPIO_RST_LCD_Pin GPIO_PIN_2
#define GPIO_RST_LCD_GPIO_Port GPIOD
#define GPIO_D_C_LCD_Pin GPIO_PIN_3
#define GPIO_D_C_LCD_GPIO_Port GPIOD
#define GPIO_CS_LCD_Pin GPIO_PIN_4
#define GPIO_CS_LCD_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */
#define FIRMWARE_VERSION                                                       \
    ((MAJOR_VERSION & 0xff) << 24 | (MINOR_VERSION & 0xff) << 16 |             \
     (BUGFIX_VERSION & 0xffff))

#if defined DEVLOPEMENT_STAGE
#define USE_FULL_ASSERT
#if defined SOFTWARE_TESTING
#define TX_BUFFER_SIZE_DEBUG_SERIAL (256)
#define LOG_BUILD_LEVEL LOG_LV_DEBUG
#else
#define TX_BUFFER_SIZE_DEBUG_SERIAL (256)
#define LOG_BUILD_LEVEL LOG_LV_DEBUG
#endif

#define TX_BUFFER_SIZE_DEBUG_SERIAL (256)
#define RX_BUFFER_SIZE_DEBUG_SERIAL (10)
#else
// #define NDEBUG
#define LOG_BUILD_LEVEL LOG_LV_DEBUG
#define TX_BUFFER_SIZE_DEBUG_SERIAL (256)
#define RX_BUFFER_SIZE_DEBUG_SERIAL (10)
#endif

#include "cAssert.h"
#include "cmsis_os2.h"
#include "stm32f3xx_hal.h"
#include <assert.h>
#include <stdint.h>

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
