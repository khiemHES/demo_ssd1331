#include "cSubject.h"
#include <assert.h>
#include <string.h>

void Init_cSubject(cSubject *me) { memset(me, 0, sizeof(cSubject)); }
int Setup_cSubject(cSubject *me, Notify_Observer_fp cb) {
    Attach_cSubject(me, cb);
    return 0;
}
int Attach_cSubject(cSubject *me, Notify_Observer_fp callback) {
    int ret = 0;
    if (me->numObserver <= MAX_NUMBER_OBSERVER) {
        me->olist[me->numObserver] = callback;
        me->numObserver++;
    } else {
        ret = 1;
    }
    return ret;
}
int Detach_cSubject(cSubject *me, Notify_Observer_fp callback) {
    int ret = 1;
    for (int i = 0; i < me->numObserver; i++) {
        if (me->olist[i] == callback) {
            me->olist[i] = NULL;
            ret = 0;
        }
        if (ret == 0) {
            if (i == me->numObserver - 1) {
                me->olist[i] = NULL;
            } else {
                me->olist[i] = me->olist[i + 1];
            }
        }
    }
    return ret;
}
void Notify_cSubject(cSubject *me, void *param) {
    for (int i = 0; i < me->numObserver; i++) {
        Notify_Observer_fp fp = me->olist[i];
        if (fp) {
            (*fp)(param);
        }
    }
}
