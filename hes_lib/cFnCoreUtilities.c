#include "cFnCoreUtilities.h"
#include "ctype.h"
#include <assert.h>

void ConvertToLowerCase_cFnCoreUtilities(int8_t *buffer, int32_t length) {
    while (length) {
        buffer[length - 1] = tolower(buffer[length - 1]);
        length--;
    }
}
void SimpleDelay_cFnCoreUtilities(int32_t count) {
    while (count--)
        ;
}
float_t LimitToFloatRange_cFnCoreUtilities(float_t input, float_t min,
                                           float_t max) {
    if (input > max)
        input = max;
    if (input < min)
        input = min;
    return input;
}
int LimitToIntRange_cFnCoreUtilities(int input, int min, int max) {
    if (input > max)
        input = max;
    if (input < min)
        input = min;
    return input;
}
uint32_t LimitToURange_cFnCoreUtilities(uint32_t input, uint32_t min,
                                        uint32_t max) {
    if (input > max)
        input = max;
    if (input < min)
        input = min;
    return input;
}
float_t Mapf_cFnCoreUtilities(float_t x, float_t X1, float_t X2, float_t Y1,
                              float_t Y2) {
    if (X1 == X2) {
        return (Y2 - Y1) / 2;
    } else
        return (x - X1) * (Y2 - Y1) / (X2 - X1) + Y1;
}
int MapI_cFnCoreUtilities(int x, int X1, int X2, int Y1, int Y2) {
    return (int)Mapf_cFnCoreUtilities((float)x, (float)X1, (float)X2, (float)Y1,
                                      (float)Y2);
}
int16_t BitConverterToInt16_cFnCoreUtilities(uint8_t *byte, int32_t index) {
    int16_t ret = ((int16_t)byte[index]) + ((int16_t)byte[index + 1] << 8);
    return ret;
}
int32_t BitConverterToInt32_cFnCoreUtilities(uint8_t *byte, int32_t index) {
    int32_t ret =
        (((int32_t)byte[index]) + ((int32_t)byte[index + 1] << 8) +
         ((int32_t)byte[index + 2] << 16) + ((int32_t)byte[index + 3] << 24));
    return ret;
}
float_t BitConverterToFloat_cFnCoreUtilities(uint8_t *byte, int32_t index) {
    float_t ret = (float_t)(
        ((uint32_t)byte[index]) + ((uint32_t)byte[index + 1] << 8) +
        ((uint32_t)byte[index + 1] << 16) + ((uint32_t)byte[index + 1] << 24));
    return ret;
}
/**
 * @brief binary search an interger value in a table. the search value is called
 * needle. the lookup table is called basket. the search always return the index
 * of the next smaller value
 *
 * @param retIndex the neareast position of the needle value in the table
 * @param needleVal the search value
 * @param basket lookup table
 * @param basketSize size of the table
 * @return int
 *  0=found needle
 *  -1= needle out of the left boundary of the lookup table
 *  -2= needle out of the right boundary of the lookup table
 */

int BinearySearchIntTable_cFnCoreUtilities(
    size_t *retIndex,        //
    const int needleVal,     //
    const void *basket,      //
    const size_t basketSize, //
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getter) {
    assert(basket || getter || !basketSize);
    size_t left = 0;
    size_t right = basketSize;
    size_t mid = (left + right) / 2;

    if (needleVal > getter(basket, right)) {
        *retIndex = 0;
        return -2;
    } else if (needleVal < getter(basket, left)) {
        *retIndex = 0;
        return -1;
    }

    int curVal = 0;
    while (left <= right) {
        curVal = getter(basket, mid);
        if (needleVal > curVal) {
            left = mid + 1;
        } else if (needleVal < curVal) {
            right = mid - 1;
        }
        mid = (left + right) / 2;
    }
    *retIndex = mid;
    return 0;
}
void SetBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k) {
    A[k / sizeof(uint32_t)] |= 1 << (k % sizeof(uint32_t));
}
void ClearBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k) {
    A[k / sizeof(uint32_t)] &= ~(1 << (k % sizeof(uint32_t)));
}
int CheckBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k) {
    return ((A[k / sizeof(uint32_t)] & (1 << (k % sizeof(uint32_t)))) != 0);
}
