/****************************************************************/
/** @file:      cFrameMsgGetter.h
 *  @author:	DO HOANG DUY KHIEM
 *  @date:		Nov 29, 2015
 *  @version:	1.6
 *  @brief:     simple serial human command interface
 *              _CommandGetterInitialize
 *              _CommandGetter
 *  @note:
 *		_this interface used interrupt serial and the same usart port
 *for debug so its recommand to use it at low transfer rate. high transfer speed
 *need to reexamine again with the consideration of : the level of utilization
 *of sending usart debug any other IRQ that the STM need to be serve _the
 *protocol for commands is very simple (without checksum, ANSCI base) the
 *protocol format is: "?"[cmdID][para1][para2]"\r" or
 *"!"[cmd][para1][para2]"\r" every valid cmd received, get a reply. a reply
 *format is: [previousMark][previousCmdID][replyContent or NACK or ACK]
 *
 ****************************************************************/

#include "cFrameMsgGetter.h"
#include "cAssert.h"
#include <string.h>

static void ClearInputBuffer__(cFrameMsgGetter *me) {
    memset(me->input_buffer_p, 0, me->frame_size);
    me->index_buffer = 0;
}

int32_t Update_cFrameMsgGetter(cFrameMsgGetter *me, const int8_t inputChar,
                               int8_t *outputMsg) {
    int8_t ch = inputChar;

    switch (me->state) {
    case RESET_MSGGETTER_STATE:
        me->index_buffer = 0;
        if (ch == '?' || ch == '!') {
            ClearInputBuffer__(me);
            me->input_buffer_p[me->index_buffer] = ch;
            me->index_buffer++;
            me->state = GETTING_MSGGETTER_STATE;
        }
        break;

    case GETTING_MSGGETTER_STATE:
        if (ch != '\0') {
            me->input_buffer_p[me->index_buffer] = ch;
            me->index_buffer++;
            if (me->index_buffer >= me->frame_size) {
                memset(me->input_buffer_p, 0, me->frame_size);
                me->state = RESET_MSGGETTER_STATE;
            } else if (ch == '\r') {
                me->input_buffer_p[me->index_buffer++] =
                    '\0'; // to replace '\r'
                memcpy(outputMsg, me->input_buffer_p, me->index_buffer);
                me->state = RESET_MSGGETTER_STATE;
                int32_t ret = me->index_buffer;
                ClearInputBuffer__(me);
                return ret;
            }
        }
        break;
    } /* switch */
    return 0;
}
int32_t Update_cFrameMsgGetter1(cFrameMsgGetter *me, const int8_t inputChar) {
    int8_t ch = inputChar;

    switch (me->state) {
    case RESET_MSGGETTER_STATE:
        me->index_buffer = 0;
        if (ch == '?' || ch == '!' || ch == '^' || ch == '~') {
            ClearInputBuffer__(me);
            me->input_buffer_p[me->index_buffer] = ch;
            me->index_buffer++;
            me->state = GETTING_MSGGETTER_STATE;
        }
        break;

    case GETTING_MSGGETTER_STATE:
        if (ch != '\0') {
            me->input_buffer_p[me->index_buffer] = ch;

            if (ch == '\r') {
                me->input_buffer_p[me->index_buffer] = '\0'; // to replace '\r'
                me->state = RETURN_MSGGETTER_STATE;
            }
            me->index_buffer++;
        }
        break;

    case RETURN_MSGGETTER_STATE:
        memcpy(me->output_frame_p, me->input_buffer_p, me->index_buffer);
        me->state = RESET_MSGGETTER_STATE;
        int32_t ret = me->index_buffer;
        ClearInputBuffer__(me);
        return ret;
    } /* switch */
    return 0;
}
int32_t Update_cFrameMsgGetter2(cFrameMsgGetter *me, const int8_t inputChar) {
    int8_t ch = inputChar;

    switch (me->state) {
    case RESET_MSGGETTER_STATE:
        me->state = GETTING_MSGGETTER_STATE;

    case GETTING_MSGGETTER_STATE:
        if (ch != '\0') {
            me->input_buffer_p[me->index_buffer] = ch;

            if (ch == '\r') {
                me->input_buffer_p[me->index_buffer] = '\0'; // to replace '\r'
                me->state = RETURN_MSGGETTER_STATE;
            }
            me->index_buffer++;
        }
        break;

    case RETURN_MSGGETTER_STATE:
        memcpy(me->output_frame_p, me->input_buffer_p, me->index_buffer);
        me->state = RESET_MSGGETTER_STATE;
        int32_t ret = me->index_buffer;
        ClearInputBuffer__(me);
        return ret;
    } /* switch */
    return 0;
}

void Setup_cFrameMsgGetter(cFrameMsgGetter *me, int32_t frame_size,
                           int8_t *input_buffer_p, int8_t *output_frame_p) {
    me->frame_size = frame_size;
    me->input_buffer_p = input_buffer_p;
    me->output_frame_p = output_frame_p;
    ClearInputBuffer__(me);
    me->state = RESET_MSGGETTER_STATE;
}
void Init_cFrameMsgGetter(cFrameMsgGetter *me) {
    memset(me, 0, sizeof(cFrameMsgGetter));
}
