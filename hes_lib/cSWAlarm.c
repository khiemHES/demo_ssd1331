#include "cSWAlarm.h"
#include <string.h>
#define ALARM_IS_RUNNING_MASK (0x01u)

void Init_cSWAlarm(cSWAlarm *me) { memset(me, 0, sizeof(cSWAlarm)); }
int Setup_cSWAlarm(cSWAlarm *me, uint32_t duration, func_cSWAlarm getTick,
                   func_cSWAlarm onAlarm) {
    SetDuration_cSWAlarm(me, duration);
    me->getTick = getTick;
    me->onAlarm = onAlarm;
    return 0;
}
int UpdateNCheck_cSWAlarm(cSWAlarm *me) {
    if (!IsRunning_cSWAlarm(me)) {
        return 0;
    }
    int ret = 0;
    uint32_t *const c = &me->Mixed_AlarmTime_and_RunState__;
    if (me->getTick) {
        uint32_t currentTime = me->getTick(me->usrParam);
        uint32_t deltaTime = (currentTime - me->lastTime);
        if (deltaTime > me->duration) {
            *c = 1;
        }
    } else {
        *c -= 2;
    }
    if (*c == 1) {
        if (me->onAlarm) {
            me->onAlarm(me->usrParam);
        }
        switch (me->type) {
        case ONETIME_TypeID_cSWAlarm:
            me->duration = 0;
            *c = 0;
            break;
        case PERIODIC_TypeID_cSWAlarm:
            Start_cSWAlarm(me);
            break;
        }
        ret = 1;
    }
    return ret;
}
int IsRunning_cSWAlarm(cSWAlarm *me) {
    uint32_t temp = me->Mixed_AlarmTime_and_RunState__ & ALARM_IS_RUNNING_MASK;
    if (temp) {
        return 1;
    } else {
        return 0;
    }
}
int Start_cSWAlarm(cSWAlarm *me) {
    if (me->duration == 0) {
        return -1;
    }
    if (!IsRunning_cSWAlarm(me)) {
        uint32_t temp = me->duration << 1 | ALARM_IS_RUNNING_MASK;
        if (temp == 0) {
            temp = 2;
        }
        me->Mixed_AlarmTime_and_RunState__ = temp;
    }
    me->lastTime = me->getTick(me->usrParam);
    return 0;
}
void Stop_cSWAlarm(cSWAlarm *me) { me->Mixed_AlarmTime_and_RunState__ = 0; }
void SetDuration_cSWAlarm(cSWAlarm *me, uint32_t duration) {
    me->duration = duration;
}
