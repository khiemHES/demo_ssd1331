#ifndef _cFrameMsgGetter_h
#define _cFrameMsgGetter_h
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @file cFrameMsgGetter.h
 * @author DO HOANG DUY KHIEM
 * @brief search for a frame from a input stream. the frame has this format
 * `?xxx\r`
 * @version 0.1
 * @date 2018-12-12
 *
 * @copyright Copyright (c) 2018
 *
 */
#include <stdint.h>

typedef enum {
    RESET_MSGGETTER_STATE = 0,
    GETTING_MSGGETTER_STATE,
    RETURN_MSGGETTER_STATE
} State_cFrameMsgGetter_e;

typedef struct FrameMsgGetter_t {
    int32_t frame_size;
    int8_t *input_buffer_p;
    int8_t *output_frame_p;
    int32_t index_buffer;
    State_cFrameMsgGetter_e state;
} cFrameMsgGetter;

void Init_cFrameMsgGetter(cFrameMsgGetter *me);
void Setup_cFrameMsgGetter(cFrameMsgGetter *me, int32_t frame_size,
                           int8_t *input_buffer_p, int8_t *output_frame_p);

/**
 * @brief Call periodically to process the new char, output a frame when having
 * a full frame
 *
 * @param me
 * @param inputChar new char
 * @param outputMsg buffer to store the output
 * @return int32_t length of the frameMsg
 */
int32_t Update_cFrameMsgGetter(cFrameMsgGetter *me, const int8_t inputChar,
                               int8_t *outputMsg);
int32_t Update_cFrameMsgGetter1(cFrameMsgGetter *me, const int8_t inputChar);
int32_t Update_cFrameMsgGetter2(cFrameMsgGetter *me, const int8_t inputChar);
#ifdef __cplusplus
}
#endif
#endif
