#ifndef _CTHERMOCOUPLE_H
#define _CTHERMOCOUPLE_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <math.h>

typedef enum ErrorCode_cThermocouple_t{
    NO_ErrorCode_cThermocouple,
    INVALID_INPUT_ErrorCode_cThermocouple,
    BINARY_SEARCH_FAILED_ErrorCode_cThermocouple,
    NUM_ErrorCode_cThermocouple,
}ErrorCode_cThermocouple;


int VoltageToTempPoli_M200_1372_TypeK_cThermocouple(long double *retTemp,
                                                    long double EmV_f);
int TempToVoltagePoli_M200_1372_TypeK_cThermocouple(long double *retVol,
                                                    long double temp);

int VoltageToTempLUTable_0oC_TypeK_cThermocouple(float_t measuredVolt_mV,
                                                 float_t *retTemp_oC);
int TempToVoltageLUTable_0oC_TypeK_cThermocouple(float_t measuredTemp_oC,
                                                 float_t *retVolt_mV);
int VoltageToTempLUTable_TypeK_cThermocouple(float_t measuredVol_mV,
                                             float_t refTemp_oC,
                                             float_t *retTemp_oC);

#ifdef __cplusplus
}
#endif
#endif
