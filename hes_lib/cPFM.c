/**
 * @brief PFM = pulse frequency modulation. generate a pulse at a certain
 * period.
 *      It uses timer to generate a short pulse and system clock to handle
 * the period of the output. the super loop will check the sys clock to
 * trigger the pulse state machine. At the right time, it calculates the right
 * OCR value for falling_edge pulse, set OC interrupt and raise the pin high. It
 * is the rising edge of PFM. The timer keeps counting up until output compare
 * occurs. At the time, the output is pull low which results in the falling edge
 * of PFM. the module is not suitable to generate signal in us range as it uses
 * sysClk to handle the period.
 *
 * @file cPFM.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-31
 */
#include "cPFM.h"
#include "main.h"
#include <assert.h>
#include <string.h>

typedef enum {
    READY_cPFM = 0,
    START_PULSE_cPFM,
    NUM_Statemachine_cPFM
} Statemachine_cPFM;

static uint32_t map_oc_timer_cPFM[][3] = {
    {TIM_CHANNEL_1, 0x34, TIM_FLAG_CC1},
    {TIM_CHANNEL_2, 0x38, TIM_FLAG_CC2},
    {TIM_CHANNEL_3, 0x3C, TIM_FLAG_CC3},
    {TIM_CHANNEL_4, 0x40, TIM_FLAG_CC4},
};
/**
 * @brief helper function to map TimerChannel_cPFM to Timer_channel, timer,
 * mask,...
 *
 * @param cc
 * @return uint32_t
 */
uint32_t GetChannelID_cPFM(TimerChannel_cPFM cc) {
    return map_oc_timer_cPFM[cc][0];
}
/**
 * @brief helper function to map TimerChannel_cPFM to Timer_channel, timer,
 * mask,...
 *
 * @param cc
 * @return uint32_t
 */
uint32_t GetOCInterruptMask_cPFM(TimerChannel_cPFM cc) { //
    return map_oc_timer_cPFM[cc][2];
}
/**
 * @brief helper function to set the cnt on OCR register
 *
 * @param me
 * @param cnt
 */
void SetCCRx_cPFM(cPFM *me, uint32_t cnt) {
    uint32_t *ccr = (uint32_t *)((uint32_t)me->fastHtimer->Instance +
                                 map_oc_timer_cPFM[me->cc][1]);
    *ccr = cnt;
}
/**
 * @brief private function. it is called when the Update_cPFM want to set timer
 * to pulse
 *
 * @param me
 */
void SetPulse_cPFM(cPFM *me) {
    HAL_TIM_OC_Stop_IT(me->fastHtimer, me->ocChannel);
    __HAL_TIM_CLEAR_FLAG(me->fastHtimer, me->ocItMask);
    uint32_t cnt = (uint32_t)(__HAL_TIM_GET_COUNTER(me->fastHtimer));
    cnt += me->onCounts;
    cnt = cnt % (__HAL_TIM_GET_AUTORELOAD(me->fastHtimer));
    SetCCRx_cPFM(me, cnt);
    HAL_TIM_OC_Start_IT(me->fastHtimer, me->ocChannel);
    HAL_GPIO_WritePin(me->port, me->pin, GPIO_PIN_SET);
    me->totalPulses++;
}
void ResetPulse_cPFM(cPFM *me) {
    HAL_TIM_OC_Stop_IT(me->fastHtimer, me->ocChannel);
    HAL_GPIO_WritePin(me->port, me->pin, GPIO_PIN_RESET);
}
void Init_cPFM(cPFM *me) { memset(me, 0, sizeof(cPFM)); }
void Setup_cPFM(cPFM *me, int id, uint32_t onCounts, uint32_t totalCounts,
                uint32_t timeout) {
    me->state = READY_cPFM;
    me->id = id;
    SetTotalCounts_cPFM(me, totalCounts);
    SetOnCounts_cPFM(me, onCounts);
    me->timeout = timeout;
}
/**
 * @brief set output compare timer channel ID
 *
 * @param me
 * @param cc
 */
void SetCC_cPFM(cPFM *me, TimerChannel_cPFM cc) {
    me->cc = cc;
    me->ocChannel = GetChannelID_cPFM(me->cc);
    me->ocItMask = GetOCInterruptMask_cPFM(me->cc);
}
int SetTotalCounts_cPFM(cPFM *me, uint32_t totalCounts) {
    int ret = 0;
    me->totalCounts = totalCounts;
    return ret;
}
uint32_t GetTotalCounts_cPFM(cPFM *me) { return me->totalCounts; }
int SetOnCounts_cPFM(cPFM *me, uint32_t onCounts) {
    int ret = 0;
    me->onCounts = onCounts;
    return ret;
}
uint32_t GetOnCounts_cPFM(cPFM *me) { return me->onCounts; }

int Update_cPFM(cPFM *me) {
    int ret = 0;
    if (me->totalCounts != 0) {
        uint32_t currentTime = me->GetTick();
        uint32_t deltaTime = currentTime - me->lastTimeTriggerPulse;
        if (deltaTime > me->totalCounts) {
            if (HAL_GPIO_ReadPin(me->port, me->pin) != GPIO_PIN_RESET) {
                ResetPulse_cPFM(me);
                assert(0);
            }
            SetPulse_cPFM(me);
            me->lastTimeTriggerPulse = currentTime;
        } else if (deltaTime > me->timeout) {
            ResetPulse_cPFM(me);
            me->error_code = PULSE_SET_TIMEOUT_cPFM;
        }
    }
    return ret;
}
