/****************************************************************/
/** @file:    cAssert.h
 *  @author:	DO HOANG DUY KHIEM
 *  @date:		12/7
 *  @version:	1.1
 *  @brief:   define assert method
 *
 *  @note:
 *      - REFERENCE: https://gist.github.com/fclairamb/7441750
 *
 ****************************************************************/
#ifndef _cAssert_h
#define _cAssert_h
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <stdint.h>
#include <stdio.h>

#define CASSERT(predicate, file) _impl_CASSERT_LINE(predicate, __LINE__, file)

#define _impl_PASTE(a, b) a##b
#define _impl_CASSERT_LINE(predicate, line, file)                              \
    typedef char _impl_PASTE(assertion_failed_##file##_,                       \
                             line)[2 * !!(predicate)-1];

#if ASSERTLV == 0
#define REQUIRE(test_)
#define ENSURE(test_)
#define INVARIANT(test_)
#define ERROR(test_)
#define ALLEGE(test_)
#elif ASSERTLV == 1
#define REQUIRE(test_) (my_assert_param(test_)) // PRECONDITION
#define ENSURE(test_)                                                          \
    my_assert_param(test_) // CONDITIONS MAY MEET, BUT NEED TO ENSURE
#define INVARIANT(test_)
#define ERROR(test_)
#define ALLEGE(test_)
#elif ASSERTLV == 2
#define REQUIRE(test_) (my_assert_param(test_)) // PRECONDITION
#define ENSURE(test_)                                                          \
    (my_assert_param(test_)) // CONDITIONS MAY MEET, BUT NEED TO ENSURE
#define INVARIANT(test_) (my_assert_param(test_)) // MUST BE ALWAYS TRUE
#define ERROR(test_) (my_assert_param(test_))     // IF IT HAS ANY ERROR
#define ALLEGE(test_)                                                          \
    (my_assert_param(test_)) // THIS CASE NOT ALLOW TO HAPPEN PRACTICALLY
#endif                       /* USE_FULL_ASSERT */
#ifdef USE_MY_ASSERT
#define my_assert_param(expr)                                                  \
    ((expr) ? (void)0                                                          \
            : my_assert_failed((uint8_t *)__FILE__, __LINE__, 0,               \
                               0)) //(int8_t *)#expr
void my_assert_failed(int8_t *file, int32_t line, uint32_t fileIndex,
                      const int8_t *expression);
///@ref:http://www.barrgroup.com/Embedded-Systems/How-To/Use-Assert-Macro
#define compile_time_assert(cond) (extern int8_t assertion[(cond) ? 1 : -1])
#else
#define my_assert_param(expr) ((void)0)
#endif

typedef enum DEBUG_LOG_LEVEL_t {
    LOG_LV_NONE,
    LOG_LV_ERROR,
    LOG_LV_WARNING,
    LOG_LV_INFO,
    LOG_LV_LOG,
    LOG_LV_DEBUG,
    LOG_LV_VERBOSE,
    NUM_DEBUG_LOG_LEVEL
} DEBUG_LOG_LEVEL;

#ifndef LOG_BUILD_LEVEL
#ifdef NDEBUG
#define LOG_BUILD_LEVEL LOG_LV_INFO
#else
#define LOG_BUILD_LEVEL NUM_DEBUG_LOG_LEVEL
#endif
#endif
extern uint8_t debug_run_level;
// The LOG_BUILD_LEVEL defines what will be compiled in the executable, in
// production it should be set to LOG_LV_INFO

#ifndef NDEBUG
#define IS_LOGGABLE(level) (level <= LOG_BUILD_LEVEL && level < debug_run_level)
#define DEBUG(level, fmt, ...)                                                 \
    do {                                                                       \
        if (IS_LOGGABLE(level)) {                                              \
            if (level == LOG_LV_ERROR) {                                       \
                fprintf(stderr, "DB%d:" fmt "\r\n", level, ##__VA_ARGS__);     \
            } else {                                                           \
                printf("DB%d:" fmt "\r\n", level, ##__VA_ARGS__);              \
            }                                                                  \
        }                                                                      \
    } while (0)
void DebugLogHandle(int32_t debugLv, int8_t *fmt, ...);
#else
#define DEBUG(level, fmt, ...) ((void)0)
#endif

#ifdef __cplusplus
}
#endif
#endif /* ifndef _cAssert_h */
