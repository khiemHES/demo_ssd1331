#include "cAssert.h"
#include "assert.h"
#include <stdarg.h>

uint8_t debug_run_level = NUM_DEBUG_LOG_LEVEL;

#ifdef USE_MY_ASSERT
void my_assert_failed(int8_t *file, int32_t line, uint32_t fileIndex,
                      const int8_t *expression) {
    (void)printf("Wrong parameters value: file %s on line %d\r\n", (int8_t *)file,
           (int32_t)line);
}
#endif

void DebugLogHandle(int32_t debugLv, int8_t *fmt, ...) {
    int32_t ret = 0;
    va_list args;
    va_start(args, fmt);
		ret = printf("DB%d:", debugLv);
    ret = printf((char*) fmt, args);
    assert(ret >= 0);
    va_end(args);
    (void)ret;
}
