#include "cSerialHAL.h"
#include <assert.h>
#include <string.h>

void Init_cSerialHAL(cSerialHAL *me) { memset(me, 0, sizeof(cSerialHAL)); }
void DeInit_cSerialHAL(cSerialHAL *me) {
    osStatus_t ret = osOK;
    ret = osSemaphoreDelete(me->txCpltFlag);
    me->txCpltFlag = 0;
    ret = osMutexDelete(me->putMsgLock);
    me->putMsgLock=0;
    ret = osMessageQueueDelete(me->rxQueue);
    me->rxQueue=0;
    ret = osMessageQueueDelete(me->txQueue);
    me->txQueue = 0;
}
int Setup_cSerialHAL(cSerialHAL *me, UART_HandleTypeDef *huart,
                     size_t txMgCount, size_t rxMgCount) {
    int ret = 0;
    me->driver = huart;
    me->txCpltFlag = osSemaphoreNew(1, 1, NULL);
    me->putMsgLock = osMutexNew(NULL);
    me->rxQueue = osMessageQueueNew(rxMgCount, sizeof(int8_t), NULL);
    me->txQueue = osMessageQueueNew(txMgCount, sizeof(int8_t), NULL);
    if (me->rxQueue && me->txQueue && me->txCpltFlag && me->driver) {
        me->txBlockTime = DEFAULT_BLOCK_TIME_cSerialHAL;
        me->rxBlockTime = DEFAULT_BLOCK_TIME_cSerialHAL;
#ifdef INTERRUPT_SERIAL_FEATURE_cSerialHAL
        HAL_UART_Receive_IT(me->driver, (uint8_t *)&me->rxChar,
                            sizeof(uint8_t));
#endif
        ret = 1;
    }
    return ret;
}
int IsReadable_cSerialHAL(cSerialHAL *me) {
    return (int)(osMessageQueueGetCount(me->rxQueue) > 0);
}
int IsWritable_cSerialHAL(cSerialHAL *me) {
    return (int)(osMessageQueueGetSpace(me->txQueue) > 0);
}
int IsSendable_cSerialHAL(cSerialHAL *me) {
    return (int)(osMessageQueueGetCount(me->txQueue) > 0);
}
int GetChar_cSerialHAL(cSerialHAL *me, uint8_t *rxChar_p) {
    int ret = 0;
    if (IsReadable_cSerialHAL(me)) {
        int ret1 =
            osMessageQueueGet(me->rxQueue, rxChar_p, NULL, me->rxBlockTime);
        if (ret1 == osOK) {
            ret = 1;
        }
    } else {
        ret = 0;
    }
    return ret;
}
void HandleRxCpltCallback_cSerialHAL(cSerialHAL *me) {
    if (osMessageQueueGetSpace(me->rxQueue)) {
        int ret = osMessageQueuePut(me->rxQueue, &me->rxChar, NULL, 0);
        if (ret != osOK) {
            me->error_code = QUEUE_OTHER_ERROR_cSerialHAL;
        }
        HAL_UART_Receive_IT(me->driver, (uint8_t *)&me->rxChar,
                            sizeof(uint8_t));
    } else {
        me->error_code = QUEUE_NO_SPACE_cSerialHAL;
        // assert(0);//TODO
    }
}
int PutChar_cSerialHAL(cSerialHAL *me, uint8_t txChar) {
    int ret = 0;
    if (IsWritable_cSerialHAL(me)) {
        int ret1 =
            osMessageQueuePut(me->txQueue, &txChar, NULL, me->txBlockTime);
        if (ret1 != osOK) {
            me->error_code = QUEUE_OTHER_ERROR_cSerialHAL;
        } else {
            ret = 1; // sendOK
        }
    } else {
        me->error_code = QUEUE_NO_SPACE_cSerialHAL;
    }
    return ret;
}
int PutString_cSerialHAL(cSerialHAL *me, uint8_t *txStr) {
    int ret = 0;
    if (osMutexAcquire(me->putMsgLock, me->txBlockTime) == osOK) {
        while (*txStr) {
            ret = PutChar_cSerialHAL(me, *txStr++);
            if (!ret) {
                break;
            }
        }
        osMutexRelease(me->putMsgLock);
    } else {
        ret = MUTEX_PUTTING_MSG_cSerialHAL;
    }

    return ret;
}
int PutBuffer_cSerialHAL(cSerialHAL *me, uint8_t *txStr, uint32_t len) {
    int ret = 0;
    if (osMutexAcquire(me->putMsgLock, me->txBlockTime) == osOK) {
        osMutexRelease(me->putMsgLock);
        while (len--) {
            ret = PutChar_cSerialHAL(me, *txStr++);
            if (!ret) {
                break;
            }
        }
    } else {
        ret = MUTEX_PUTTING_MSG_cSerialHAL;
    }
    return ret;
}
void Update_cSerialHAL(cSerialHAL *me) {
    int ret = 0;
    // Handle sending out serial msg
    if (osMessageQueueGetCount(me->txQueue)) {
        int ret1 =
            osMessageQueueGet(me->txQueue, &me->txChar, NULL, me->txBlockTime);
        if (ret1 == osOK) {
            // yield for the tx complete
            osSemaphoreAcquire(me->txCpltFlag, osWaitForever);
            int ret2 = 0;
#ifdef INTERRUPT_SERIAL_FEATURE_cSerialHAL
            ret2 = HAL_UART_Transmit_IT(me->driver, (uint8_t *)&me->txChar,
                                        sizeof(char));
#else
            ret2 = HAL_UART_Transmit(me->driver, (uint8_t *)&me->txChar,
                                     sizeof(char), me->txBlockTime);
            osSemaphoreRelease(me->txCpltFlag);
#endif
            if (ret2 != HAL_OK) {
                me->error_code = HAL_TRANSMIT_ERROR_cSerialHAL;
            }
        } else {
            me->error_code = QUEUE_OTHER_ERROR_cSerialHAL;
        }
    }
#ifndef INTERRUPT_SERIAL_FEATURE_cSerialHAL
    ret =
        HAL_UART_Receive(me->driver, (uint8_t *)&me->rxChar, sizeof(int8_t), 0);
    if (ret == HAL_OK) {
        int ret3 =
            osMessageQueuePut(me->rxQueue, &me->rxChar, NULL, me->rxBlockTime);
        if (ret3 != osOK) {
            me->error_code = QUEUE_OTHER_ERROR_cSerialHAL;
        }
    } else if (ret == HAL_ERROR) {
        assert(0);
    }
#endif
    (void)ret;
}
void HandleTxCpltCallback_cSerialHAL(cSerialHAL *me) {
    int ret = osSemaphoreRelease(me->txCpltFlag);
    if (ret != osErrorResource) {
        me->error_code = SEMAPHORE_ERROR_cSerialHAL;
    }
}
