#include "cDebounceSwitch.h"
#include <string.h>

void Init_cDebounceSwitch(cDebounceSwitch *me) {
    memset(me, 0, sizeof(cDebounceSwitch));
}
/**
 * @brief configure cDebounceSwitch
 *
 * @param me
 * @param updatedPeriod (in ms) = 1/SamplingFreq
 * @param debounceTime (in ms)
 * @return int
 */
int Setup_cDebounceSwitch(cDebounceSwitch *me, uint32_t updatedPeriod,
                          uint32_t debounceTime) {
    if (updatedPeriod != 0) {
        me->updatedPeriod = updatedPeriod;
        me->maxIntegrator = (uint32_t)debounceTime / me->updatedPeriod;
    } else {
        me->updatedPeriod = SAMPLING_PERIOD_cDebounceSwitch;
        me->maxIntegrator =
            (uint32_t)DEBOUNCE_TIME_cDebounceSwitch / me->updatedPeriod;
    }
		return 0;
}

void Update_cDebounceSwitch(cDebounceSwitch *me) {
    uint32_t currentTime = osKernelGetTickCount();
    uint32_t deltaTime = currentTime - me->lastTime;
    if (deltaTime > me->updatedPeriod) {
        if (me->rawState == 0) {
            if (me->keyIntegrator > 0) {
                me->keyIntegrator--;
            }
        } else if (me->keyIntegrator < me->maxIntegrator)
            me->keyIntegrator++;

        if (me->keyIntegrator == 0)
            me->debouncedState = 0;
        else if (me->keyIntegrator >= me->maxIntegrator) {
            me->debouncedState = 1;
            // defensive code if integrator got corrupted
            me->keyIntegrator = me->maxIntegrator;
        }
        me->lastTime = currentTime;
    }
}
int Read_cDebounceSwitch(cDebounceSwitch *me) { return me->debouncedState; }
void SetRawState_cDebounceSwitch(cDebounceSwitch *me, int value) {
    me->rawState = (value != 0);
}
