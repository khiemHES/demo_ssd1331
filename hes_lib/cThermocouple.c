/**
 * @brief thermocouple related function
 *  - give the relationship btw temperature and miliVolt
 *  - temperature unit is oC
 *  - Emf is measured in mili Volt
 *  - all data is referenced from https://srdata.nist.gov
 * @todo
 *  - can use fix point math for this module to make it less stack usage (e.g.
 * VoltageToTempPoli_M200_1372_TypeK_cThermocouple)
 *  - use float for VoltageToTempPoli_M200_1372_TypeK_cThermocouple
 *  -
 *
 * @file cThermocouple.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-09
 */
#include "cThermocouple.h"
#include "cFnCoreUtilities.h"
#include <assert.h>

typedef struct cPoint_cThermocouple_t {
    float_t temp;
    float_t miliVolt;
} cPoint_cThermocouple;

/**
 * @brief formular to convert voltage diff of thermocouple to temperature
 * @ref https://srdata.nist.gov/its90/type_k/kcoefficients_inverse.html
 *  (-200, -5.891)
 *  ( 0, 0.000)
 *  (500, 20.644)
 *  (1370, 54.819)
 *
 */
#define VOLT_TO_TEMP_l_M200_0_TypeK_cThermocouple(EmV)                         \
    (2.5173462e1 * pow((EmV), 1) - 1.1662878e0 * pow((EmV), 2) -               \
     1.0833638e0 * pow((EmV), 3) - 8.9773540e-1 * pow((EmV), 4) -              \
     3.7342377e-1 * pow((EmV), 5) - 8.6632643e-2 * pow((EmV), 6) -             \
     1.0450598e-2 * pow((EmV), 7) - 5.1920577e-4 * pow((EmV), 8))
#define VOLT_TO_TEMP_l_0_500_TypeK_cThermocouple(EmV)                          \
    (2.508355e1 * pow((EmV), 1) + 7.860106e-2 * pow((EmV), 2) -                \
     2.503131e-1 * pow((EmV), 3) + 8.315270e-2 * pow((EmV), 4) -               \
     1.228034e-2 * pow((EmV), 5) + 9.804036e-4 * pow((EmV), 6) -               \
     4.413030e-5 * pow((EmV), 7) + 1.057734e-6 * pow((EmV), 8) -               \
     1.052755e-8 * pow((EmV), 9))
#define VOLT_TO_TEMP_l_500_1372_TypeK_cThermocouple(EmV)                       \
    (-1.318058e2 + 4.830222e1 * pow((EmV), 1) - 1.646031e0 * pow((EmV), 2) +   \
     5.464731e-2 * pow((EmV), 3) - 9.650715e-4 * pow((EmV), 4) +               \
     8.802193e-6 * pow((EmV), 5) - 3.110810e-8 * pow((EmV), 6))
#define TEMP_TO_VOLT_l_M270_0_TypeK_cThermocouple(temp)                        \
    (0.39450128025e-1 * pow((temp), 1) + 0.236223735980e-4 * pow((temp), 2) -  \
     0.328589067840e-6 * pow((temp), 3) - 0.499048287770e-8 * pow((temp), 4) - \
     0.675090591730e-10 * pow((temp), 5) -                                     \
     0.574103274280e-12 * pow((temp), 6) -                                     \
     0.310888728940e-14 * pow((temp), 7) -                                     \
     0.104516093650e-16 * pow((temp), 8) -                                     \
     0.198892668780e-19 * pow((temp), 9) -                                     \
     0.163226974860e-22 * pow((temp), 10))
#define TEMP_TO_VOLT_l_0_1372_TypeK_cThermocouple(temp)                        \
                                                                               \
    (-0.176004136860e-1 + 0.389212049750e-1 * pow((temp), 1) +                 \
     0.185587700320e-4 * pow((temp), 2) - 0.994575928740e-7 * pow((temp), 3) + \
     0.318409457190e-9 * pow((temp), 4) -                                      \
     0.560728448890e-12 * pow((temp), 5) +                                     \
     0.560750590590e-15 * pow((temp), 6) -                                     \
     0.320207200030e-18 * pow((temp), 7) +                                     \
     0.971511471520e-22 * pow((temp), 8) -                                     \
     0.121047212750e-25 * pow((temp), 9) +                                     \
     0.118597600000e0 *                                                        \
         exp(-0.118343200000e-3 * pow((temp)-0.126968600000e3, 2)))

#define VOLT_TO_TEMP_f_M200_0_TypeK_cThermocouple(EmV)                         \
    (2.5173462 * pow((EmV), 1) * 10 - 1.1662878 * pow((EmV), 2) -              \
     1.0833638 * pow((EmV), 3) - 8.9773540 * pow((EmV), 4) / 10 -              \
     3.7342377 * pow((EmV), 5) / 10 - 8.6632643 * pow((EmV), 6) / 100 -        \
     1.0450598 * pow((EmV), 7) / 100 - 5.1920577 * pow((EmV), 8) / 10000)
#define VOLT_TO_TEMP_f_0_500_TypeK_cThermocouple(EmV)                          \
    (2.508355e1 * pow((EmV), 1) * 10 + 7.860106 * pow((EmV), 2) / 100 -        \
     2.503131 * pow((EmV), 3) / 10 + 8.315270 * pow((EmV), 4) / 100 -          \
     1.228034 * pow((EmV), 5) / 100 + 9.804036 * pow((EmV), 6) / 10000 -       \
     4.413030 * pow((EmV), 7) / 100000 + 1.057734 * pow((EmV), 8) / 1000000 -  \
     1.052755 * pow((EmV), 9) / 10000000)
#define VOLT_TO_TEMP_f_500_1372_TypeK_cThermocouple(EmV)                       \
    (-1.318058e2 + 4.830222 * pow((EmV), 1) * 10 - 1.646031 * pow((EmV), 2) +  \
     5.464731 * pow((EmV), 3) / 100 - 9.650715 * pow((EmV), 4) / 1000 +        \
     8.802193 * pow((EmV), 5) / 1000000 - 3.110810 * pow((EmV), 6) / 1000000)
#define TEMP_TO_VOLT_f_M270_0_TypeK_cThermocouple(temp)                        \
    (0.39450128025 * pow((temp), 1) / 10 +                                     \
     0.236223735980 * pow((temp), 2) / 10000 -                                 \
     0.328589067840 * pow((temp), 3) / 1000000 -                               \
     0.499048287770 * pow((temp), 4) / 100000000 -                             \
     0.675090591730 * pow((temp), 5) / 10000000000 -                           \
     0.574103274280 * pow((temp), 6) / 1000000000000 -                         \
     0.310888728940 * pow((temp), 7) / 100000000000000 -                       \
     0.104516093650 * pow((temp), 8) / 10000000000000000 -                     \
     0.198892668780 * pow((temp), 9) / 10000000000000000000 -                  \
     0.163226974860 * pow((temp), 10) / 10000000000000000000000)
#define TEMP_TO_VOLT_f_0_1372_TypeK_cThermocouple(temp)                        \
                                                                               \
    (-0.176004136860 / 10 + 0.389212049750 * pow((temp), 1) / 10 +             \
     0.185587700320 * pow((temp), 2) / 10000 -                                 \
     0.994575928740 * pow((temp), 3) / 10000000 +                              \
     0.318409457190 * pow((temp), 4) / 1000000000 -                            \
     0.560728448890 * pow((temp), 5) / 1000000000000 +                         \
     0.560750590590 * pow((temp), 6) / 1000000000000000 -                      \
     0.320207200030 * pow((temp), 7) / 1000000000000000000 +                   \
     0.971511471520 * pow((temp), 8) / 10000000000000000000000 -               \
     0.121047212750 * pow((temp), 9) / 10000000000000000000000000 +            \
     0.118597600000e0 *                                                        \
         exp(-0.118343200000e-3 * pow((temp)-0.126968600000e3, 2)))
static const cPoint_cThermocouple table_for_poli_method_cThermocouple[] = {
    {-200, -5.891}, {0, 0}, {500, 20.644}, {1370, 54.819} //
};
int VoltageToTempPoli_M200_1372_TypeK_cThermocouple(long double *retTemp,
                                                    long double EmV_f) {
    int ret = 0;
    long double temperature = 0;
    if (EmV_f < table_for_poli_method_cThermocouple[0].miliVolt) {
        ret = INVALID_INPUT_ErrorCode_cThermocouple;
    } else if (EmV_f <= table_for_poli_method_cThermocouple[1].miliVolt) {
        temperature = VOLT_TO_TEMP_l_M200_0_TypeK_cThermocouple(EmV_f);
    } else if (EmV_f <= table_for_poli_method_cThermocouple[2].miliVolt) {
        temperature = VOLT_TO_TEMP_l_0_500_TypeK_cThermocouple(EmV_f);
    } else if (EmV_f <= table_for_poli_method_cThermocouple[3].miliVolt) {
        temperature = VOLT_TO_TEMP_l_500_1372_TypeK_cThermocouple(EmV_f);
    } else {
        ret = INVALID_INPUT_ErrorCode_cThermocouple;
    }
    if (ret == 0) {
        *retTemp = temperature;
    } else {
        *retTemp = 0;
    }
    return ret;
}
int TempToVoltagePoli_M200_1372_TypeK_cThermocouple(long double *retVol,
                                                    long double temp) {
    int ret = 0;
    long double voltage = 0;
    if (temp < table_for_poli_method_cThermocouple[0].temp) {
        ret = INVALID_INPUT_ErrorCode_cThermocouple;
    } else if (temp <= table_for_poli_method_cThermocouple[1].temp) {
        voltage = TEMP_TO_VOLT_l_M270_0_TypeK_cThermocouple(temp);
    } else if (temp <= table_for_poli_method_cThermocouple[3].temp) {
        voltage = TEMP_TO_VOLT_l_0_1372_TypeK_cThermocouple(temp);
    } else {
        ret = INVALID_INPUT_ErrorCode_cThermocouple;
    }
    if (ret == 0) {
        *retVol = voltage;
    } else {
        *retVol = 0;
    }
    return ret;
}
/**
 * @brief lookupTable to find the temperature from voltage
 * @ref https://srdata.nist.gov/its90/download/type_k.tab
 *
 */
static cPoint_cThermocouple lookupTable_0oC_cThermocouple[] = {
    {-270, -6.458}, {-260, -6.441}, {-250, -6.404}, {-240, -6.344},
    {-230, -6.262}, {-220, -6.158}, {-210, -6.035}, {-200, -5.891},
    {-190, -5.730}, {-180, -5.550}, {-170, -5.354}, {-160, -5.141},
    {-150, -4.913}, {-140, -4.669}, {-130, -4.411}, {-120, -4.138},
    {-110, -3.852}, {-100, -3.554}, {-90, -3.243},  {-80, -2.920},
    {-70, -2.587},  {-60, -2.243},  {-50, -1.889},  {-40, -1.527},
    {-30, -1.156},  {-20, -0.778},  {-10, -0.392},  {0, 0.0},
    {10, 0.397},    {20, 0.798},    {30, 1.203},    {40, 1.612},
    {50, 2.023},    {60, 2.436},    {70, 2.851},    {80, 3.267},
    {90, 3.682},    {100, 4.096},   {110, 4.509},   {120, 4.920},
    {130, 5.328},   {140, 5.735},   {150, 6.138},   {160, 6.540},
    {170, 6.941},   {180, 7.340},   {190, 7.739},   {200, 8.138},
    {210, 8.539},   {220, 8.940},   {230, 9.343},   {240, 9.747},
    {250, 10.153},  {260, 10.561},  {270, 10.971},  {280, 11.382},
    {290, 11.795},  {300, 12.209},  {310, 12.624},  {320, 13.040},
    {330, 13.457},  {340, 13.874},  {350, 14.293},  {360, 14.713},
    {370, 15.133},  {380, 15.554},  {390, 15.975},  {400, 16.397},
    {410, 16.820},  {420, 17.243},  {430, 17.667},  {440, 18.091},
    {450, 18.516},  {460, 18.941},  {470, 19.366},  {480, 19.792},
    {490, 20.218},  {500, 20.644},  {510, 21.071},  {520, 21.497},
    {530, 21.924},  {540, 22.350},  {550, 22.776},  {560, 23.203},
    {570, 23.629},  {580, 24.055},  {590, 24.480},  {600, 24.905},
    {610, 25.330},  {620, 25.755},  {630, 26.179},  {640, 26.602},
    {650, 27.025},  {660, 27.447},  {670, 27.869},  {680, 28.289},
    {690, 28.710},  {700, 29.129},  {710, 29.548},  {720, 29.965},
    {730, 30.382},  {740, 30.798},  {750, 31.213},  {760, 31.628},
    {770, 32.041},  {780, 32.453},  {790, 32.865},  {800, 33.275},
    {810, 33.685},  {820, 34.093},  {830, 34.501},  {840, 34.908},
    {850, 35.313},  {860, 35.718},  {870, 36.121},  {880, 36.524},
    {890, 36.925},  {900, 37.326},  {910, 37.725},  {920, 38.124},
    {930, 38.522},  {940, 38.918},  {950, 39.314},  {960, 39.708},
    {970, 40.101},  {980, 40.494},  {1000, 41.276}, {1010, 41.665},
    {1020, 42.053}, {1030, 42.440}, {1040, 42.826}, {1050, 43.211},
    {1060, 43.595}, {1070, 43.978}, {1080, 44.359}, {1090, 44.740},
    {1100, 45.119}, {1110, 45.497}, {1120, 45.873}, {1130, 46.249},
    {1140, 46.623}, {1150, 46.995}, {1160, 47.367}, {1170, 47.737},
    {1180, 48.105}, {1190, 48.473}, {1200, 48.838}, {1210, 49.202},
    {1220, 49.565}, {1230, 49.926}, {1240, 50.286}, {1250, 50.644},
    {1260, 51.000}, {1270, 51.355}, {1280, 51.708}, {1290, 52.060},
    {1300, 52.410}, {1310, 52.759}, {1320, 53.106}, {1330, 53.451},
    {1340, 53.795}, {1350, 54.138}, {1360, 54.479}, {1370, 54.819} //
};

#define SIZE_LOOKUP_TABLE_0_1327_oC_TypeK_cThermocouple                        \
    (sizeof(lookupTable_0oC_cThermocouple) / sizeof(cPoint_cThermocouple))

/**
 * @brief BinearySearch requires lookup value is as integer
 *  but temperature and voltage is in float_t. so both of them will be
 * *1000 before feed into binary search
 *
 */
#define SCALE_cThermocouple (1000)
#define SCALE_UP_i_cThermocouple(x) ((int32_t)((x)*SCALE_cThermocouple))
#define SCALE_DOWN_f_cThermocouple(x) ((x)*1.0f / SCALE_cThermocouple)

int GetValue1FromTable_cThermocouple(const void *basket, const size_t index) {
    cPoint_cThermocouple *pt = &(((cPoint_cThermocouple *)basket)[index]);
    return SCALE_UP_i_cThermocouple(pt->miliVolt);
}
int GetValue0FromTable_cThermocouple(const void *basket, const size_t index) {
    cPoint_cThermocouple *pt = &(((cPoint_cThermocouple *)basket)[index]);
    return SCALE_UP_i_cThermocouple(pt->temp);
}
int TempToVoltageLUTable_0oC_TypeK_cThermocouple(float_t measuredTemp_oC,
                                                 float_t *retVolt_mV) {
    if (measuredTemp_oC == 0) {
        *retVolt_mV = 0;
        return 0;
    }

    int ret = 0;
    cPoint_cThermocouple *table_p = lookupTable_0oC_cThermocouple;
    int32_t needleX = SCALE_UP_i_cThermocouple(measuredTemp_oC);
    int32_t needleY = 0;
    size_t index = 0;
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getX =
        GetValue0FromTable_cThermocouple;
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getY =
        GetValue1FromTable_cThermocouple;
    index = 0;
    ret = BinearySearchIntTable_cFnCoreUtilities(
        &index, needleX, table_p,
        SIZE_LOOKUP_TABLE_0_1327_oC_TypeK_cThermocouple, getX);
    if (ret != 0) {
        ret = BINARY_SEARCH_FAILED_ErrorCode_cThermocouple;
        return ret;
    }

    needleY = MapI_cFnCoreUtilities(
        needleX, getX(table_p, index), getX(table_p, index + 1),
        getY(table_p, index), getY(table_p, index + 1));
    *retVolt_mV = SCALE_DOWN_f_cThermocouple(needleY);
    return ret;
}
int VoltageToTempLUTable_0oC_TypeK_cThermocouple(float_t measuredVolt_mV,
                                                 float_t *retTemp_oC) {
    if (measuredVolt_mV == 0) {
        *retTemp_oC = 0;
        return 0;
    }

    int ret = 0;
    cPoint_cThermocouple *table_p = lookupTable_0oC_cThermocouple;
    int32_t needleX = SCALE_UP_i_cThermocouple(measuredVolt_mV);
    int32_t needleY = 0;
    size_t index = 0;
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getX =
        GetValue1FromTable_cThermocouple;
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getY =
        GetValue0FromTable_cThermocouple;

    index = 0;
    ret = BinearySearchIntTable_cFnCoreUtilities(
        &index, needleX, table_p,
        SIZE_LOOKUP_TABLE_0_1327_oC_TypeK_cThermocouple,
        GetValue1FromTable_cThermocouple);
    if (ret != 0) {
        ret = BINARY_SEARCH_FAILED_ErrorCode_cThermocouple;
        return ret;
    }

    needleY = MapI_cFnCoreUtilities(
        needleX, getX(table_p, index), getX(table_p, index + 1),
        getY(table_p, index), getY(table_p, index + 1));
    *retTemp_oC = SCALE_DOWN_f_cThermocouple(needleY);
    return ret;
}
int VoltageToTempLUTable_TypeK_cThermocouple(float_t measuredVol_mV,
                                             float_t refTemp_oC,
                                             float_t *retTemp_oC) {

    int ret = 0;
    float_t refVolt_mV = 0;

    int ret1 = 0;

    ret1 =
        TempToVoltageLUTable_0oC_TypeK_cThermocouple(refTemp_oC, &refVolt_mV);
    if (ret1 != 0) {
        ret = ret1;
        return ret;
    }
    // todo
    float test = measuredVol_mV + refVolt_mV;
    float_t retTemp_oC_ = 0;
    ret1 = VoltageToTempLUTable_0oC_TypeK_cThermocouple(test, &retTemp_oC_);
    if (ret1 != 0) {
        ret = ret1;
        return ret;
    } else {
        *retTemp_oC = retTemp_oC_;
    }
    return ret;
}
