#ifndef _CPT1000_H
#define _CPT1000_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <math.h>

int32_t ResistanceToTempLUTable_cPt1000(float_t Res, float_t *retTemp_oC);

#ifdef __cplusplus
}
#endif
#endif
