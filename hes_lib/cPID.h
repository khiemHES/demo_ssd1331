#ifndef _CPID_H
#define _CPID_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <math.h>
typedef struct {
    int32_t id;
    int32_t enable_flag;
    uint32_t sampleTime;
    // pid state
    float_t setPoint; // Last setpoint
    float_t pvar;
    float_t dState, iState;
    float_t pTerm_1, iTerm_1, dTerm_1;
    float_t pidOut;
    // PID para
    float_t iGain; // integral gain
    float_t pGain; // proportional gain
    float_t dGain; // derivative gain
    float_t fGain; // low pass filter factor (1 - pole) for derivative gain
    float_t pMax, iMax, dMax, oMax;
    float_t iStateMax;
    float_t delKp; // handle Unbalanced Output Drive
    float_t Kz;    // feed forward compensation
} cPID;

void SetPID_cPID(cPID *me, float_t p, float_t i, float_t d);
void SetLimits_cPID(cPID *me, float_t ipMax, float_t iiMax, float_t idMax, float_t ioMax);
void SetSamplingTime_cPID(cPID *me, uint32_t newSampleTime);
void ResetPID_cPID(cPID *me, float_t currentPos, float_t output);

void Init_cPID(cPID *me);
void Setup_cPID(cPID *me, float_t p, float_t i, float_t d, float_t f, float_t ipMax,
                float_t iiMax, float_t iiStateMax, float_t idMax, float_t ioMax,
                float_t idelKp, float_t iKz);
float_t Update_cPID(cPID *me, float_t setpoint, float_t pvar);
float_t Update_cPID2(cPID *me, float_t setpoint, float_t pvar);
#ifdef __cplusplus
}
#endif
#endif /* ifndef _cAssert_h */
