/**
 * @brief
 *
 * @ref
 * 		ref: http://www.mstarlabs.com/apeng/techniques/pidsoftw.html
 * @file cPID.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-19
 */

#include "cPID.h"
#include "cFnCoreUtilities.h"
#include "main.h"
#include "string.h"
#include <assert.h>
#include <stdlib.h>

void Init_cPID(cPID *me) {
    memset(me, 0, sizeof(cPID));
    me->pMax = 1000;
    me->iMax = 1000;
    me->iStateMax = 1000;
    me->dMax = 1000;
    me->oMax = 1000;
}
void SetPID_cPID(cPID *me, float_t p, float_t i, float_t d) {
    me->pGain = p;
    me->iGain = i;
    me->dGain = d;
}
void SetLimits_cPID(cPID *me, float_t ipMax, float_t iiMax, float_t idMax, float_t ioMax) {
    me->pMax = ipMax;
    me->iMax = iiMax;
    me->dMax = idMax;
    me->oMax = ioMax;
}
void SetSamplingTime_cPID(cPID *me, uint32_t newSampleTime) {
    if (newSampleTime > 0) {
        float_t ratio = ((float_t)newSampleTime) / me->sampleTime;
        me->iGain *= ratio;
        me->dGain /= ratio;
        me->sampleTime = newSampleTime;
    }
}
void ResetPID_cPID(cPID *me, float_t currentPos, float_t output) {
    me->dTerm_1 = currentPos;
    me->iTerm_1 = output;
    LimitToFloatRange_cFnCoreUtilities(me->iTerm_1, -me->iMax, me->iMax);
}
void Setup_cPID(cPID *me, float_t p, float_t i, float_t d, float_t f, float_t ipMax,
                float_t iiMax, float_t iiStateMax, float_t idMax, float_t ioMax,
                float_t idelKp, float_t iKz) {
    SetPID_cPID(me, p, i, d);
    SetLimits_cPID(me, ipMax, iiMax, idMax, ioMax);
    me->iStateMax = iiStateMax;
    me->fGain = f;
}

// http://www.mstarlabs.com/apeng/techniques/pidsoftw.html
float_t Update_cPID(cPID *me, float_t setpoint, float_t pvar) {
    float_t p = me->pGain;
    float_t i = me->iGain;
    float_t d = me->dGain;
    float_t f = (me->fGain) ? me->fGain : 1.0f;

    me->setPoint = setpoint;
    me->pvar = pvar;
    float_t error = setpoint - pvar;

    // calculate the proportional term
    me->pTerm_1 = p * error;
    if (me->pTerm_1 > me->pMax) {
        me->pTerm_1 = me->pMax;
    } else if (me->pTerm_1 < -me->pMax) {
        me->pTerm_1 = -me->pMax;
    }

    // calculate the integral state with appropriate limiting
    me->iState += error;
    // this integrator Rate Limiting to avoid windup
    if (me->iState > me->iStateMax)
        me->iState = me->iStateMax;
    else if (me->iState < -me->iStateMax)
        me->iState = -me->iStateMax;
    // this integrator latching to avoid windup
    me->iTerm_1 = i * me->iState;
    if (me->iTerm_1 > me->iMax) {
        me->iTerm_1 = me->iMax;
        me->iState -= error;
    } else if (me->iTerm_1 < -me->iMax) {
        me->iTerm_1 = -me->iMax;
        me->iState -= error;
    }

    // derivative
    if (me->dGain) {
        error = -pvar;
        me->dTerm_1 = (d * f) * (error - me->dState);
        me->dState += f * (error - me->dState);
        if (me->dTerm_1 > me->dMax) {
            me->dTerm_1 = me->dMax;
        } else if (me->dTerm_1 < -me->dMax) {
            me->dTerm_1 = -me->dMax;
        }
    } else {
        me->dTerm_1 = 0.0f;
    }

    me->pidOut = me->pTerm_1 + me->iTerm_1 + me->dTerm_1;

    if (me->pidOut > me->oMax) {
        me->pidOut = me->oMax;
    } else if (me->pidOut < -me->oMax) {
        me->pidOut = -me->oMax;
    }
    return me->pidOut;
}
float_t Update_cPID2(cPID *me, float_t setpoint, float_t pvar) {
    float_t p = me->pGain;
    float_t i = me->iGain;
    float_t d = me->dGain;
    float_t f = (me->fGain) ? me->fGain : 1.0f;

    me->setPoint = setpoint;
    me->pvar = pvar;
    float_t error = setpoint - pvar;

    // calculate the proportional term
    me->pTerm_1 = p * error;
    LimitToFloatRange_cFnCoreUtilities(me->pTerm_1, -me->pMax, me->pMax);

    // integration
    if (i) {
        me->iTerm_1 += i * error;
        LimitToFloatRange_cFnCoreUtilities(me->iTerm_1, -me->iMax, me->iMax);
    } else {
        me->iTerm_1 = 0;
    }
    // derivative
    if (d) {
        error = -pvar;
        me->dTerm_1 = (d) * (pvar - me->dState);
        me->dState = pvar;
        LimitToFloatRange_cFnCoreUtilities(me->dTerm_1, -me->dMax, me->dMax);
    } else {
        me->dTerm_1 = 0.0f;
    }
    me->pidOut = me->pTerm_1 + me->iTerm_1 + me->dTerm_1;
    LimitToFloatRange_cFnCoreUtilities(me->pidOut, -me->oMax, me->oMax);
    (void)f;
    return me->pidOut;
}
