#ifndef _CDEBOUNCESWITCH_H
#define _CDEBOUNCESWITCH_H
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief using integrator algorithm to debound switch
 *
 * @file cDebounceSwitch.h
 * @author DO HOANG DUY KHIEM
 * @date 2018-09-02
 * @ref
 *  http://www.kennethkuhn.com/electronics/debounce.c
 *  https://hackaday.com/2010/11/09/debounce-code-one-post-to-rule-them-all/
 */

/**
 * @note : explain integrator algorithm
 *  real signal
 * 0000111111110000000111111100000000011111111110000000000111111100000 corrupted
 * 0100111011011001000011011010001001011100101111000100010111011100010
 *  integrator
 * 0100123233233212100012123232101001012321212333210100010123233321010 output
 * 0000001111111111100000001111100000000111111111110000000001111111000
 */

#include "main.h"

#define SAMPLING_PERIOD_cDebounceSwitch (10) // in ms
#define DEBOUNCE_TIME_cDebounceSwitch (1000) // in ms

/**
 * @brief how to use
    cDebounceSwitch ds;
    while(1){
        int state = ReadSwitch();
        SetRawState_cDebounceSwitch(&ds, staet);
        Update_cDebounceSwitch(&ds);
        if(Read_cDebounceSwitch(&db)=1){
            //button on , do smt here
        }
    }
 */

/**
 * @brief debounce switch controller
 *
 */
typedef struct cDebounceSwitch_t {
    int id;
    uint32_t error_code;

    int32_t rawState;
    int32_t debouncedState; // hold the debounce state of btn
    uint32_t keyIntegrator;
    uint32_t maxIntegrator;
    uint32_t lastTime; // last time Loop executed
    uint32_t updatedPeriod;
} cDebounceSwitch;

void Init_cDebounceSwitch(cDebounceSwitch *me);
int Setup_cDebounceSwitch(cDebounceSwitch *me, uint32_t updatedPeriod,
                          uint32_t debounceTime);
/**
 * @brief Call periodically to process raw state and determine debounced state
 *
 * @param me
 */
void Update_cDebounceSwitch(cDebounceSwitch *me);
/**
 * @brief Read the current debounced state of the switch
 *
 * @param me
 * @return int
 */
int Read_cDebounceSwitch(cDebounceSwitch *me);
/**
 * @brief Set the raw state for later process
 *
 * @param me
 * @param value
 */
void SetRawState_cDebounceSwitch(cDebounceSwitch *me, int value);
#ifdef __cplusplus
}
#endif
#endif
