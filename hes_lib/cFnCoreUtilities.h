#ifndef _cFnCoreUtilities_h
#define _cFnCoreUtilities_h
#ifdef __cplusplus
extern "C" {
#endif
#include "stdint.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SWAP_UINT16T_cFnCoreUtilities(x)                                       \
    ((((x) >> 8) & 0x00ff) | (((x) << 8) & 0xff00))
#define STRINGIFY_cFnCoreUtilities(x) #x
#define TOSTRING_cFnCoreUtilities(x) STRINGIFY_cFnCoreUtilities(x)
#define WRITE_REGISTER_cFnCoreUtilities(var)                                   \
    (__asm volatile("mov " TOSTRING(var) ", %[" TOSTRING(var) "]"              \
                                                              "\n\t"           \
                    :                                                          \
                    : [var] "r"(var)))
#define READ_REGISTER_cFnCoreUtilities(var)                                    \
    (__asm volatile("mov %[" TOSTRING(var) "], " TOSTRING(var) "\n\t"          \
                    : [var] "=r"(var)))

#define NELEM_cFnCoreUtilities(x) (sizeof(x) / sizeof(x[0]))
#define MAX_cFnCoreUtilities(x, y) (((x) > (y)) ? (x) : (y))
#define MIN_cFnCoreUtilities(x, y) (((x) > (y)) ? (y) : (x))
/* a=target variable, b=bit number to act upon 0-n */
#define BIT_SET_cFnCoreUtilities(a, b) ((a) |= (1 << (b)))
#define BIT_CLEAR_cFnCoreUtilities(a, b) ((a) &= ~(1 << (b)))
#define BIT_FLIP_cFnCoreUtilities(a, b) ((a) ^= (1 << (b)))
#define BIT_CHECK_cFnCoreUtilities(a, b) ((a) & (1 << (b)))

/* x=target variable, y=mask */
#define BITMASK_SET_cFnCoreUtilities(x, y) ((x) |= (y))
#define BITMASK_CLEAR_cFnCoreUtilities(x, y) ((x) &= (~(y)))
#define BITMASK_FLIP_cFnCoreUtilities(x, y) ((x) ^= (y))
#define BITMASK_CHECK_cFnCoreUtilities(x, y) (((x) & (y)) == (y))

void ConvertToLowerCase_cFnCoreUtilities(int8_t *buffer, int32_t length);
void Checksum32B_cFnCoreUtilities(void);
void SimpleDelay_cFnCoreUtilities(int32_t count);
float_t LimitToFloatRange_cFnCoreUtilities(float_t input, float_t min,
                                           float_t max);
uint32_t LimitToURange_cFnCoreUtilities(uint32_t input, uint32_t min,
                                        uint32_t max);
int LimitToIntRange_cFnCoreUtilities(int input, int min, int max);
float_t Mapf_cFnCoreUtilities(float_t value, float_t fromLow, float_t fromHigh,
                              float_t toLow, float_t toHigh);
int MapI_cFnCoreUtilities(int x, int X1, int X2, int Y1, int Y2);
int16_t BitConverterToInt16_cFnCoreUtilities(uint8_t *byte, int32_t index);
int32_t BitConverterToInt32_cFnCoreUtilities(uint8_t *byte, int32_t index);
float_t BitConverterToFloat_cFnCoreUtilities(uint8_t *byte, int32_t index);

typedef int (*GetIntValue_BinearySearchIntTable_cFnCoreUtilities)(
    const void *basket, const size_t bIndex);
int BinearySearchIntTable_cFnCoreUtilities(
    size_t *retIndex, const int needleVal, const void *basket,
    const size_t basketSize,
    GetIntValue_BinearySearchIntTable_cFnCoreUtilities getter);

void SetBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k);
void ClearBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k);
int CheckBitArray_cFnCoreUtilities(uint32_t A[], uint32_t k);
#ifdef __cplusplus
}
#endif
#endif
