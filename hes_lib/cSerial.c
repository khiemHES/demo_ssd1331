/**
 * @brief provide a simplified interfaces to control serial peripheral.
 *
 * ##FEATURES:
 * - interrupt
 * - queue btw main and interrupt
 * - thread-safe
 * - CONS
 *   - if multiple threads print at the same time, the output will be mixed
 *
 * ##HOW TO USE
 * - need to put Update_cSerial in a cyclic task
 * - configure peripheral in cube
 * - add HandleRxCpltCallback_cSerial and HandleTxCpltCallback_cSerial to
 * the HandleRxCpltCallback of the application project
 * - call Setup_cSerial.
 *
 * ##TODO
 * - include the peripheral configuration of UART1, UART2, UART3
 *
 * @file SerialHAL.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-01
 */
#include "cSerial.h"
#include <assert.h>
#include <string.h>

void Init_cSerial(cSerial *me) { memset(me, 0, sizeof(cSerial)); }
int Setup_cSerial(cSerial *me, UART_HandleTypeDef *huart,
                     size_t txMgCount, size_t rxMgCount) {
    int ret = 0;
    me->driver = huart;
    me->txCpltFlag = osSemaphoreNew(1, 1, NULL);
    me->rxQueue = osMessageQueueNew(rxMgCount, sizeof(int8_t), NULL);
    me->txQueue = osMessageQueueNew(txMgCount, sizeof(int8_t), NULL);
    if (me->rxQueue && me->txQueue && me->txCpltFlag && me->driver) {
        me->txBlockTime = DEFAULT_BLOCK_TIME_cSerial;
        me->rxBlockTime = DEFAULT_BLOCK_TIME_cSerial;
#ifdef INTERRUPT_SERIAL_FEATURE_cSerial
        HAL_UART_Receive_IT(me->driver, (uint8_t *)&me->rxChar,
                            sizeof(uint8_t));
#endif
        ret = 1;
    }
    return ret;
}
/**
 * @brief
 *
 * @param me
 * @param rxChar_p
 * @return int
 *      1=successfully get 1 char
 *      0=fail to get char
 */
int GetChar_cSerial(cSerial *me, uint8_t *rxChar_p) {
    int ret = 0;
    if (osMessageQueueGetCount(me->rxQueue)) {
        int ret1 =
            osMessageQueueGet(me->rxQueue, rxChar_p, NULL, me->rxBlockTime);
        if (ret1 == osOK) {
            ret = 1;
        }
    } else {
        ret = 0;
    }
    return ret;
}
/**
 * @brief will be called inside ISR when a receive serial s completed
 *
 * @param me
 */
void HandleRxCpltCallback_cSerial(cSerial *me) {
    if (osMessageQueueGetSpace(me->rxQueue)) {
        int ret = osMessageQueuePut(me->rxQueue, &me->rxChar, NULL, 0);
        if (ret != osOK) {
            me->error_code = QUEUE_OTHER_ERROR_cSerial;
        }
        HAL_UART_Receive_IT(me->driver, (uint8_t *)&me->rxChar,
                            sizeof(uint8_t));
    } else {
        me->error_code = QUEUE_NO_SPACE_cSerial;
        // assert(0);//TODO
    }
}
/**
 * @brief
 *
 * @param txChar
 * @return int
 *      1= sucessfuly put a char into queue
 *      0= fail to put char
 */
int PutChar_cSerial(cSerial *me, uint8_t txChar) {
    int ret = 0;
    int spaceLeft = GetNumberByteLeft_cRingBuffer(&me->txRBuff);
    if (spaceLeft) {
        Push_cRingBuffer(&me->txRBuff, &txChar);
        ret = 1; // sendOK
    } else {
        me->error_code = QUEUE_NO_SPACE_cSerial;
    }
    return ret;
}
int PutString_cSerial(cSerial *me, uint8_t *txStr) {
    int ret = 0;
    while (*txStr) {
        ret = PutChar_cSerial(me, *txStr++);
        if (!ret) {
            break;
        }
    }
    return ret;
}
/**
 * @brief cyclic routine to handle the txRBuff.
 *      if have any char in queue, it will request interrupt to send the txChar
 * out
 *
 * @param me
 */
void Update_cSerial(cSerial *me) {
    int ret = 0;
    int usedSpace = GetNumberByteUsed_cRingBuffer(&me->txRBuff);
    int txCpltState = osSemaphoreGetCount(me->txCpltFlag);
    if (usedSpace && txCpltState) {
        osSemaphoreAcquire(me->txCpltFlag, osWaitForever);
        int ret1 = Pop_cRingBuffer(&me->txRBuff, &me->txChar);
        if (ret1 == osOK) {
            // yield for the tx complete
            int ret2 = 0;
#ifdef INTERRUPT_SERIAL_FEATURE_cSerial
            ret2 = HAL_UART_Transmit_IT(me->driver, (uint8_t *)&me->txChar,
                                        sizeof(char));
#else
            ret2 = HAL_UART_Transmit(me->driver, (uint8_t *)&me->txChar,
                                     sizeof(char), me->txBlockTime);
            osSemaphoreRelease(me->txCpltFlag);
#endif
            if (ret2 != HAL_OK) {
                me->error_code = HAL_TRANSMIT_ERROR_cSerial;
            }
        } else {
            me->error_code = QUEUE_OTHER_ERROR_cSerial;
        }
    }
#ifndef INTERRUPT_SERIAL_FEATURE_cSerial
    ret =
        HAL_UART_Receive(me->driver, (uint8_t *)&me->rxChar, sizeof(int8_t), 0);
    if (ret == HAL_OK) {
        int ret3 =
            osMessageQueuePut(me->rxQueue, &me->rxChar, NULL, me->rxBlockTime);
        if (ret3 != osOK) {
            me->error_code = QUEUE_OTHER_ERROR_cSerial;
        }
    } else if (ret == HAL_ERROR) {
        assert(0);
    }
#endif
    (void)ret;
}
/**
 * @brief be called in ISR, to signal the super loop that it has finished
 * sending
 *
 * @param me
 */
void HandleTxCpltCallback_cSerial(cSerial *me) {
    int ret = osSemaphoreRelease(me->txCpltFlag);
    if (ret != osOK) {
        me->error_code = SEMAPHORE_ERROR_cSerial;
    }
}
