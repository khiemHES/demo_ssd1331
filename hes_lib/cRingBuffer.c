#include "cRingBuffer.h"
#include <assert.h>

void Init_cRingBuffer(cRingBuffer *me) { //
    memset(me, 0, sizeof(cRingBuffer));
}
void Setup_cRingBuffer(cRingBuffer *me, uint32_t numberItems,
                       uint32_t sizeItem) {
    assert(me != 0);
    assert(sizeItem != 0);
    assert(numberItems != 0);
    assert(me->buffer != 0);
    me->itemSize = (int32_t)sizeItem;
    me->bufferSize = (int32_t)(numberItems * sizeItem);
}

void Push_cRingBuffer(cRingBuffer *me, void *item) {
    assert(me != 0);
    assert(item != 0);

    me->head += me->itemSize;
    if (me->circularEnable) {
        me->head %= me->bufferSize;
    } else {
        assert(me->head <= me->bufferSize);
    }

    (void *)memcpy((void *)&me->buffer[me->head], (void *)item, me->itemSize);

    me->active += me->itemSize;
    if (me->circularEnable) {
        if (me->active < me->bufferSize) {
            me->active += me->itemSize;
        } else {
            me->tail += me->itemSize;
            me->tail %= me->bufferSize;
        }
    } else {
        assert(me->active <= me->bufferSize);
    }
}
int32_t Pop_cRingBuffer(cRingBuffer *me, void *item) {
    assert(me != 0);
    assert(item != 0);

    if (!me->active) {
        return -1;
    }

    int32_t lastTail = me->tail;
    me->tail += me->itemSize;
    if (me->circularEnable) {
        me->tail %= me->bufferSize;
    } else {
        assert(me->tail <= me->bufferSize);
    }

    (void *)memcpy((void *)item, (void *)&me->buffer[lastTail], me->itemSize);
    me->active -= me->itemSize;

    return 0;
}

void *PeekTailItemPtr_cRingBuffer(cRingBuffer *me) {
    return (void *)((uint32_t)me->buffer + (uint32_t)me->tail);
}
void *PeekHeadItemPtr_cRingBuffer(cRingBuffer *me) {
    return (void *)((uint32_t)me->buffer + (uint32_t)me->head);
}
void *PeekNextItemPtr_cRingBuffer(cRingBuffer *me, void *currentPtr) {
    // currentPtr must be a multiple of me->itemSize
    int32_t byteDistance = (int32_t)((uint32_t)currentPtr - (uint32_t)me->buffer);
    (void)byteDistance;

    void *tempPtr = (void *)((uint32_t)currentPtr + (uint32_t)me->itemSize);
    if ((uint32_t)tempPtr >=
        ((uint32_t)me->buffer + (uint32_t)me->bufferSize)) {
        tempPtr = (void *)((uint32_t)tempPtr - me->bufferSize);
    }

    if (((uint32_t)tempPtr - (uint32_t)me->buffer) == ((uint32_t)me->head)) {
        tempPtr = 0;
    }

    return tempPtr;
}

int32_t GetNumberItemLeft_cRingBuffer(cRingBuffer *me) {
    return (me->bufferSize - me->active) / me->itemSize;
}
int32_t GetNumberItemUsed_cRingBuffer(cRingBuffer *me) {
    return me->active / me->itemSize;
}
int32_t GetNumberByteLeft_cRingBuffer(cRingBuffer *me) {
    return me->bufferSize - me->active;
}
int32_t GetNumberByteUsed_cRingBuffer(cRingBuffer *me) { //
    return me->active;
}
void Flush_cRingBuffer(cRingBuffer *me) {
    me->head = me->tail;
    me->active = 0;
}
