
#ifndef __CSTATEMACHINE_H_
#define __CSTATEMACHINE_H_

#include <stdint.h>

#define cStateMachine_NULL_STATE (0)
enum cStateMachine_ReservedSignals_e {
    cStateMachine_NULL_SIG = 0, // underscore prefix = private
    cStateMachine_ENTRY_SIG,    /*!< signal when entering new state */
    cStateMachine_EXIT_SIG,     /*!< signal when exiting old state */
    cStateMachine_INIT_SIG,     /*!< signal when initialization */
    cStateMachine_USER_SIG = 4  /*!< user signal */
};

typedef struct cStateMachine_Event_t {
    uint32_t sig; // to identify signal type
    int id;       // for dynamic identification
    uint32_t value;
} cStateMachine_Event;

typedef struct cStateMachine_AlarmEvt_t {
    cStateMachine_Event super;
    int id;
    uint32_t duration;
    uint32_t start_time;
    uint32_t end_time;
} cStateMachine_AlarmEvt;

typedef struct cStateMachine_SwitchEvt_t {
    cStateMachine_Event super;
    int id;
    int curr_state;
    int last_state;
} cStateMachine_SwitchEvt;

typedef enum cStateMachine_StateAction_e {
    cStateMachine_HOLD_STATE,
    cStateMachine_CHANGE_STATE,
}cStateMachine_StateAction;

struct cStateMachine_t;
typedef cStateMachine_StateAction (*cStateMachine_StateHandler)(
    void *stateInfo, cStateMachine_Event *e);

typedef struct cStateMachine_t {
    int id;
    int curr_state;
    cStateMachine_StateHandler curr_handler;
    int last_state_;
    cStateMachine_StateHandler last_handler_;
} cStateMachine;

int cStateMachineF_Init(cStateMachine *me);
void cStateMachineF_Initialize(cStateMachine *me, cStateMachine_Event *e);
void cStateMachineF_Handler(cStateMachine *me, cStateMachine_Event *e);
cStateMachine_StateAction
cStateMachineF_SetState(cStateMachine *me, int newState,
                        cStateMachine_StateHandler newHandler);
int cStateMachineF_GetLastState(cStateMachine *me);

#endif
