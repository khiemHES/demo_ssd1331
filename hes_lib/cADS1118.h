#ifndef _CADS1118_H
#define _CADS1118_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <math.h>

#define ADS1118_CONST_6_144V_LSB_mV (0.1875f)
#define ADS1118_CONST_4_096V_LSB_mV (0.125f)
#define ADS1118_CONST_2_048V_LSB_mV (0.0625f)
#define ADS1118_CONST_1_024V_LSB_mV (0.03125f)
#define ADS1118_CONST_0_512V_LSB_mV (0.015625f)
#define ADS1118_CONST_0_256V_LSB_mV (0.0078125f)

// CONFIG_BIT_RESV: default
#define ADS1118_CONFIG_BIT_RESV (1 << 0)

typedef enum {
    NO_EFFECT_ADS1118 = 0x0,          // for read
    SINGLE_CONVER_START_ADS1118 = 0x1 // for write
} ADS_SS_TypeDef;

typedef enum {
    AINPN_0_1_ADS1118 = 0x0,
    AINPN_0_3_ADS1118 = 0x1,
    AINPN_1_3_ADS1118 = 0x2,
    AINPN_2_3_ADS1118 = 0x3,
    AINPN_0_GND_ADS1118 = 0x4,
    AINPN_1_GND_ADS1118 = 0x5,
    AINPN_2_GND_ADS1118 = 0x6,
    AINPN_3_GND_ADS1118 = 0x7,
    INTERNAL_IC_TEMPERATURE_ADS1118 = 0x8,
    NUM_ADS_MUX_TypeDef,
    NO_MUX_ADS1118,
} ADS_MUX_TypeDef;

typedef enum {
    PGA_6144_ADS1118 = 0x0,
    PGA_4096_ADS1118 = 0x1,
    PGA_2048_ADS1118 = 0x2,
    PGA_1024_ADS1118 = 0x3,
    PGA_512_ADS1118 = 0x4,
    PGA_256_ADS1118 = 0x5
} ADS_PGA_TypeDef;

typedef enum {
    CONTINUOUS_ADS1118 = 0x0,
    SINGLE_SHOT_ADS1118 = 0x1
} ADS_MODE_TypeDef;

typedef enum {
    DR_8_SPS_ADS1118 = 0x0,
    DR_16_SPS_ADS1118 = 0x1,
    DR_32_SPS_ADS1118 = 0x2,
    DR_64_SPS_ADS1118 = 0x3,
    DR_128_SPS_ADS1118 = 0x4,
    DR_250_SPS_ADS1118 = 0x5,
    DR_475_SPS_ADS1118 = 0x6,
    DR_860_SPS_ADS1118 = 0x7
} ADS_DATARATE_TypeDef;

typedef enum {
    ADC_MODE_ADS1118 = 0x0,
    TEMPERATURE_MODE_ADS1118 = 0x1
} ADS_TSMODE_TypeDef;

typedef enum {
    PULL_UP_DISABLE_ADS1118 = 0x0,
    PULL_UP_ENABLE_ADS1118 = 0x1
} ADS_PULL_TypeDef;

typedef enum {
    DATA_VALID_ADS1118 = 0x1,
    DATA_INVALID_ADS1118 = 0x2
} ADS_NOP_TypeDef;

typedef enum {
    DATA_READY_ADS1118 = 0x0,
    DATA_NREADY_ADS1118 = 0x1
} ADS_RDY_TypeDef;

typedef union {
    struct {
        volatile uint8_t RESV : 1; // low
        volatile uint8_t NOP : 2;
        volatile uint8_t PULLUP : 1;
        volatile uint8_t TS_MODE : 1;
        volatile uint8_t DR : 3;
        volatile uint8_t MODE : 1;
        volatile uint8_t PGA : 3;
        volatile uint8_t MUX : 3;
        volatile uint8_t SS : 1; // high
    } fields;
    volatile uint16_t word;
    volatile uint8_t byte[2];
} ADS1118_Config_Reg;

typedef enum {
    NO_ERROR_cADS1118 = 0,
    SEMAPHORE_ERROR_cADS1118,
    HAL_SPI_ERROR_cADS1118,
    CONFIG_UPDATE_NOP_cADS1118,
    SPI_COM_TIMEOUT_ERROR_cADS1118,
    ADC_BEYOND_LOWER_BOUND_cADS1118,
    ADC_BEYOND_UPER_BOUND_cADS1118,
    TIMEOUT_SS_CONVERSION_cADS1118,
    NUM_ERRORS_cADS1118
} ErrorCode_cADS1118;

typedef enum {
    NO_CONFIGURE_cADS1118,
    TRANSMISSION_32B_cADS1118,
    TRANSMISSION_16B_cADS1118,
    NUM_StateMachine_cADS1118,
} TransmissionType_cADS1118;

typedef struct cADS1118_t {
    int id;
    uint32_t error_code;
    TransmissionType_cADS1118 transmissionType;
    // for spi communication
    uint8_t rxSpiBuffer[4];
    uint8_t txSpiBuffer[4];
    osSemaphoreId_t cpltFlag;
    SPI_HandleTypeDef *hspi;
    GPIO_TypeDef *nssPort;
    uint16_t nssPin;

    // for caller to set configuration
    ADS1118_Config_Reg configReg;
    uint32_t timeAtTransmit;
} cADS1118;

void Init_cADS1118(cADS1118 *me);
int Setup_cADS1118(cADS1118 *me);
int IsComCptl_cADS1118(cADS1118 *me);
int IsConfigUpdated_cADS1118(cADS1118 *me);
int SaveConfigReg_cADS1118(cADS1118 *me, ADS1118_Config_Reg *config);
void ChangeADCMux_cADS1118(cADS1118 *me, ADS_MUX_TypeDef mux);
void WriteNSSPin_cADS118(cADS1118 *me, int flag);
int GetNSSPinState_cADS118(cADS1118 *me, int flag);
int WriteSPI_cADS118(cADS1118 *me);
int GetRxConfig_cADS1118(cADS1118 *me, ADS1118_Config_Reg *config);
int GetICTemp_cADS1118(cADS1118 *me, float *icTemp, ADS1118_Config_Reg *config);
int GetVoltageVal_cADS1118(cADS1118 *me, float *voltageVal,
                           ADS1118_Config_Reg *config);
ADS_MUX_TypeDef GetCurrentADCMuxConfig_cADS1118(cADS1118 *me);
void HandleTxRxCpltCallback_cADS1118(cADS1118 *me);
float_t CalculateICTemp_cADS1118(uint16_t adcVal);

#ifdef __cplusplus
}
#endif
#endif
