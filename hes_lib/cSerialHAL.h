#ifndef _CSERIALHAL_H
#define _CSERIALHAL_H
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file cSerialHAL.h
 * @author DO HOANG DUY KHIEM
 * @brief provide a simplified interfaces to control serial peripheral
 * @version 0.1
 * @date 2018-12-12
 *
 * @copyright Copyright (c) 2018
 *
 * ##FEATURES:
 * - interrupt
 * - queue btw main and interrupt
 * - thread-safe
 * - CONS
 *   - if multiple threads print at the same time, the output will be mixed
 *
 *
 * ##HOW TO USE
 * - need to put Update_cSerialHAL in a cyclic task
 * - configure peripheral in cube
 * - add HandleRxCpltCallback_cSerialHAL and HandleTxCpltCallback_cSerialHAL to
 * the HandleRxCpltCallback of the application project
 * - call Setup_cSerialHAL.
 *
 * ##TODO
 * - include the peripheral configuration of UART1, UART2, UART3
 *
 *
 */

#include "main.h"

#define DEFAULT_BLOCK_TIME_cSerialHAL (100)
#define INTERRUPT_SERIAL_FEATURE_cSerialHAL

enum ErrorCode_cSerialHAL {
    NO_ERROR_cSerialHAL,
    QUEUE_NO_SPACE_cSerialHAL,
    QUEUE_OTHER_ERROR_cSerialHAL,
    SEMAPHORE_ERROR_cSerialHAL,
    MUTEX_PUTTING_MSG_cSerialHAL,
    HAL_TRANSMIT_ERROR_cSerialHAL,
    NUM_ERRORS_cSerialHAL
};

typedef struct cSerialHAL_t {
    int id;
    uint32_t error_code;
    uint32_t rxBlockTime;
    uint32_t txBlockTime;
    UART_HandleTypeDef *driver;
    char txChar;
    char rxChar;
    osSemaphoreId_t txCpltFlag;
    osMutexId_t putMsgLock;
    osMessageQueueId_t rxQueue;
    osMessageQueueId_t txQueue;
} cSerialHAL;

void Init_cSerialHAL(struct cSerialHAL_t *me);
void DeInit_cSerialHAL(struct cSerialHAL_t *me);
int Setup_cSerialHAL(cSerialHAL *me, UART_HandleTypeDef *huart,
                     size_t txMgCount, size_t rxMgCount);

/**
 * @brief Check if receive any chars
 *
 * @param me
 * @return int
 */
int IsReadable_cSerialHAL(cSerialHAL *me);
/**
 * @brief Check if any space left in txQueue for other thread to print
 *
 * @param me
 * @return int
 */
int IsWritable_cSerialHAL(cSerialHAL *me);
/**
 * @brief Check if txQueue is empty. if not, serial doesnt have to send serial
 * char
 *
 * @param me
 * @return int
 */
int IsSendable_cSerialHAL(cSerialHAL *me);
/**
 * @brief
 *
 * @param me
 * @param rxChar_p
 * @return int
 *      1=successfully get 1 char
 *      0=fail to get char
 */
int GetChar_cSerialHAL(struct cSerialHAL_t *me, uint8_t *rxChar_p);
/**
 * @brief put a char to the sendign buffer
 *
 * @param txChar
 * @return int
 *      1= sucessfuly put a char into queue
 *      0= fail to put char
 */
int PutChar_cSerialHAL(struct cSerialHAL_t *me, uint8_t txChar);
/**
 * @brief put a string to the sendign buffer
 *
 * @param me
 * @param txStr
 * @return int
 *      1= sucessfuly put a char into queue
 *      0= fail to put char
 */
int PutString_cSerialHAL(struct cSerialHAL_t *me, uint8_t *txStr);
/**
 * @brief put a buffer of a certain length to the sendign buffer
 *
 * @param me
 * @param txStr
 * @param len txStr len
 * @return int
 *      1= sucessfuly put a char into queue
 *      0= fail to put char
 */
int PutBuffer_cSerialHAL(cSerialHAL *me, uint8_t *txStr, uint32_t len);
/**
 * @brief cyclic routine to handle the txQueue.
 *      if have any char in queue, it will request interrupt to send the txChar
 * out
 *
 * @param me
 */
void Update_cSerialHAL(struct cSerialHAL_t *me);
/**
 * @brief will be called inside ISR when a receive serial s completed
 *
 * @param me
 */
void HandleRxCpltCallback_cSerialHAL(struct cSerialHAL_t *me);
/**
 * @brief be called in ISR, to signal the super loop that it has finished
 * sending
 *
 * @param me
 */
void HandleTxCpltCallback_cSerialHAL(struct cSerialHAL_t *me);

#ifdef __cplusplus
}
#endif
#endif
