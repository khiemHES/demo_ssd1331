#include "TestcFCS.h"
#include "cFCS.h"
#include "cFnCoreUtilities.h"
#include <assert.h>

static cFCS fcs__;
static cFCS *tFCS = &fcs__;

int Setup_TestcFCS(void) {
    Init_cFCS(tFCS);
    Setup_cFCS(tFCS);
    return 0;
}

void Test_GetSupPressureLevel_cFCS(void) { //
    float testVector[][2] = {{0, -3},    {0.2, -2}, {0.25, -2}, {0.35, -1},
                             {0.38, -1}, {0.4, 0},  {0.8, 0},   {1.01, 1},
                             {1.2, 1},   {1.37, 2}, {1.4, 2},   {1.5, 3},
                             {1.6, 3},   {2, 3}};
    float val = 0;
    int level = 0;
    int total = NELEM_cFnCoreUtilities(testVector);
    for (int i = 0; i < total; i++) {
        val = testVector[i][0];
        level = GetSupPressureLevel_cFCS(tFCS, val);
        assert(level == testVector[i][1]);
    }
}

void Test_GetStackTempLevel_cFCS(void) { //
    float testVector[][2] = {{0, -3}, {30, -3}, {45, 0}, {50, 0}, {60, 1},
                             {65, 1}, {67, 2},  {70, 3}, {75, 3}, {100, 3}};
    float val = 0;
    int level = 0;

    for (int i = 0; i < NELEM_cFnCoreUtilities(testVector); i++) {
        val = testVector[i][0];
        level = GetStackTempLevel_cFCS(tFCS, val);
        assert(level == testVector[i][1]);
    }
}

void Test_WriteDigitalOut_cFCS(void) { //
    int flag = 0;
    WriteDigitalOut_cFCS(tFCS, LOADSW_DigitalIO_cFCS, flag);
    int tFlag = ReadDigitalIO_cFCS(tFCS, LOADSW_DigitalIO_cFCS);
    assert(flag == tFlag);

    flag = 1;
    WriteDigitalOut_cFCS(tFCS, LOADSW_DigitalIO_cFCS, flag);
    tFlag = ReadDigitalIO_cFCS(tFCS, LOADSW_DigitalIO_cFCS);
    assert(flag == tFlag);
}

void Test_SetStackFanPwr_cFCS(void) { //
    float flag = 0;
    SetStackFanPwr_cFCS(tFCS, NUM_FanID_cFCS, flag);
    float tFlag = GetStackFanPwr_cFCS(tFCS);
    assert(flag == tFlag);

    flag = 1;
    SetStackFanPwr_cFCS(tFCS, NUM_FanID_cFCS, flag);
    tFlag = GetStackFanPwr_cFCS(tFCS);
    assert(flag == tFlag);

    flag = 0.5;
    SetStackFanPwr_cFCS(tFCS, NUM_FanID_cFCS, flag);
    tFlag = GetStackFanPwr_cFCS(tFCS);
    assert(flag == tFlag);
}
