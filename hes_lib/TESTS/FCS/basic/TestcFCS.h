#ifndef _TESTCFCS_H
#define _TESTCFCS_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

int Setup_TestcFCS(void);
void Test_GetSupPressureLevel_cFCS(void);
void Test_GetStackTempLevel_cFCS(void);

#ifdef __cplusplus
}
#endif
#endif
