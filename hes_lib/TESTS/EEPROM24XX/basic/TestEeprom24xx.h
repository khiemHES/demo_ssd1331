#ifndef _TESTEEPROM24XX_H
#define _TESTEEPROM24XX_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

int Setup_TestEeprom24xx(void);
void TestEeprom24xx(void);

#ifdef __cplusplus
}
#endif
#endif
