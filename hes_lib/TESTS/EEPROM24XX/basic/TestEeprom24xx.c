#include "TestEeprom24xx.h"
#include "cAssert.h"
#include "cFnCoreUtilities.h"
#include "eeprom24xx.h"

int Setup_TestEeprom24xx(void) { //
    return 0;
}

void TestEeprom24xx__(uint16_t startAddr) {
    DEBUG(LOG_LV_VERBOSE, "Start TestEeprom24xx");

    uint8_t WriteData[128] = {0};
    for (int i = 0; i < 128; i++) {
        WriteData[i] = i;
    }
    uint8_t ReadData[128] = {0};
    HAL_Delay(100);

    while (EEPROM24XX_IsConnected() == false) {
        HAL_Delay(100);
    }

    EEPROM24XX_Save(0xFF, WriteData, 128);
    EEPROM24XX_Load(0xFF, ReadData, 128);

    for (int i = 0; i < NELEM_cFnCoreUtilities(WriteData); i++) {
        if (WriteData[i] != ReadData[i]) {
            DEBUG(LOG_LV_ERROR, "Test FAILED");
            break;
        }
    }

    DEBUG(LOG_LV_VERBOSE, "End TestEeprom24xx");
}

void TestEeprom24xx(void) {
    // for 1st memory region = 0x00-0xff
    TestEeprom24xx__(0x0);
    TestEeprom24xx__(0x10);
    TestEeprom24xx__(0x12);
    // for 2nd memory region = 0x100-0x1ff
    TestEeprom24xx__(0x100);
    TestEeprom24xx__(0x110);
    TestEeprom24xx__(0x120);
    // cross region
    TestEeprom24xx__(0xff);
    TestEeprom24xx__(0x90);
    TestEeprom24xx__(0xe5);
}
