#include "cFnCoreUtilities.h"
#include "greentea-client/test_env.h"
#include "mbed.h"
#include "unity/unity.h"
#include "utest/utest.h"
#include <stdio.h>
#include <string.h>

using namespace utest::v1;

// Tests LimitToIntRange_cFnCoreUtilities
void Test_LimitToIntRange_cFnCoreUtilities(void) {
    int TEST_SETS[][4] = {
        {0, 0, 0, 0},    //
        {1, 0, 0, 0},    //
        {0, 0, 10, 0},   //
        {5, 0, 10, 5},   //
        {10, 0, 10, 10}, //
        {20, 0, 10, 10}, //
    };
    int nelem = sizeof(TEST_SETS) / sizeof(TEST_SETS[0]);
    for (int i = 0; i < nelem; i++) {
        TEST_ASSERT(LimitToIntRange_cFnCoreUtilities(
                        TEST_SETS[i][0], TEST_SETS[i][1], TEST_SETS[i][2]) ==
                    TEST_SETS[i][3]);
    }
}

utest::v1::status_t test_setup(const size_t number_of_cases) {
    // Setup Greentea using a reasonable timeout in seconds
    GREENTEA_SETUP(40, "default_auto");
    return verbose_test_setup_handler(number_of_cases);
}

// Test cases
Case cases[] = {
    Case("Testing LimitToIntRange_cFnCoreUtilities",
         Test_LimitToIntRange_cFnCoreUtilities),
};

utest::v1::status_t greentea_test_setup(const size_t number_of_cases) {
    GREENTEA_SETUP(30, "default_auto");
    return greentea_test_setup_handler(number_of_cases);
}

Specification specification(greentea_test_setup, cases);

int main() { //
    Harness::run(specification);
}
