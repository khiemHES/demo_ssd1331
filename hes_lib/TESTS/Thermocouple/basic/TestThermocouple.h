#ifndef _CTESTTHERMOCOUPLE_H
#define _CTESTTHERMOCOUPLE_H
#ifdef __cplusplus
extern "C" {
#endif

#include "cThermocouple.h"
#include "main.h"

void Test_VoltageToTempPoli_M200_1372_TypeK_cThermocouple(void);
void Test_TempToVoltagePoli_M200_1372_TypeK_cThermocouple(void);
void Test_VoltageToTempLUTable_0oC_TypeK_cThermocouple(void);
void Test_TempToVoltageLUTable_0oC_TypeK_cThermocouple(void);
#ifdef __cplusplus
}
#endif
#endif
