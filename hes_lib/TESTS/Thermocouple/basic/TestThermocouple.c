/**
 * @brief to test cThermocouple. Using the following point to test
 *
 *
 * @file TestThermocouple.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-09
 */

/*
equal to correct

not equal is correct
    {-280, -6.458}
    {2000, 200}
    {0, -1.0}
    {100, 7}
*/

#include "TestThermocouple.h"
#include "cFnCoreUtilities.h"
#include <assert.h>
#include <math.h>

// equal to correct
static float_t data1[][2] = {
    {-200.0, -5.891}, {-140.0, -4.669}, {0.0, 0.0},      {20.0, 0.798},
    {100.0, 4.096},   {500.0, 20.644},  {620.0, 25.755}, {1370.0, 54.819} //
};
// not equal is correct
static float_t data2[][2] = {
    {-280.0, -6.458}, {2000.0, 200.0}, {0.0, -1.0}, {100.0, 7.0} //
};
/**
 * @brief
 *  need to set OS_STACK_SIZE to at least 800 or else it will be overflowed
 *
 */
void Test_VoltageToTempPoli_M200_1372_TypeK_cThermocouple(void) {
    int32_t i = NELEM_cFnCoreUtilities(data1);
    long double result = 0;
    int32_t ret = 0;
    // equal to correct
    while (i--) {
        ret = VoltageToTempPoli_M200_1372_TypeK_cThermocouple(
            &result, data1[i][1]);
        long double delta = result - data1[i][0];
        assert(fabsl(delta) < 0.1);
        assert(ret == 0);
    }
    i = NELEM_cFnCoreUtilities(data2);
    result = 0;
    ret = 0;
    while (i--) {
        ret = VoltageToTempPoli_M200_1372_TypeK_cThermocouple(
            &result, data2[i][1]);
        long double delta = result - data2[i][0];
        assert(fabsl(delta) > 0.1);
    }
}
void Test_TempToVoltagePoli_M200_1372_TypeK_cThermocouple(void) {
    int32_t i = NELEM_cFnCoreUtilities(data1);
    long double result = 0;
    int32_t ret = 0;

    while (i--) {
        ret = TempToVoltagePoli_M200_1372_TypeK_cThermocouple(&result,
                                                              data1[i][0]);
        long double delta = result - data1[i][1];
        assert(fabsl(delta) < 0.1);
        assert(ret == 0);
    }
    i = NELEM_cFnCoreUtilities(data2);
    result = 0;
    ret = 0;
    while (i--) {
        ret = TempToVoltagePoli_M200_1372_TypeK_cThermocouple(&result,
                                                              data2[i][0]);
        long double delta = result - data2[i][1];
        assert(fabsl(delta) > 0.1);
    }
}
void Test_VoltageToTempLUTable_0oC_TypeK_cThermocouple(void) {
    int32_t i = NELEM_cFnCoreUtilities(data1);
    float_t result = 0;
    float_t delta = 0;
    int32_t ret = 0;

    while (i--) {
        ret = VoltageToTempLUTable_0oC_TypeK_cThermocouple(data1[i][1],
                                                           &result);
        delta = result - data1[i][0];
        assert(fabsf(delta) == 0);
        assert(ret == 0);
    }

    i = NELEM_cFnCoreUtilities(data2);
    result = 0;
    delta = 0;
    ret = 0;
    while (i--) {
        ret = VoltageToTempLUTable_0oC_TypeK_cThermocouple(data2[i][1],
                                                           &result);
        delta = result - data2[i][0];
        assert(fabsf(delta) != 0);
    }
}
void Test_TempToVoltageLUTable_0oC_TypeK_cThermocouple(void) {
    int32_t i = NELEM_cFnCoreUtilities(data1);
    float_t result = 0;
    float_t delta = 0;
    int32_t ret = 0;

    while (i--) {
        ret = TempToVoltageLUTable_0oC_TypeK_cThermocouple(data1[i][0],
                                                           &result);
        delta = result - data1[i][1];
        assert(fabsf(delta) == 0);
        assert(ret == 0);
    }

    i = NELEM_cFnCoreUtilities(data2);
    result = 0;
    delta = 0;
    ret = 0;
    while (i--) {
        ret = TempToVoltageLUTable_0oC_TypeK_cThermocouple(data2[i][0],
                                                           &result);
        delta = result - data2[i][1];
        assert(fabsf(delta) != 0);
    }
}
