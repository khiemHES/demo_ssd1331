#include "cFnCoreUtilities.h"
#include "cPt1000.h"
#include <assert.h>

float data1_TestcPt2000[][2] = { //
    {-200, 185.3}, {-180, 270.8},  {-70, 723.3},  {0, 1000},
    {20, 1077.90}, {100, 1385.10}, {500, 2809.80}};

void Test_ResistanceToTempLUTable_cPt1000() {
    int i = NELEM_cFnCoreUtilities(data1_TestcPt2000);
    float result = 0;
    float delta = 0;
    int ret = 0;

    while (i--) {
        ret = ResistanceToTempLUTable_cPt1000(data1_TestcPt2000[i][1], &result);
        delta = result - data1_TestcPt2000[i][0];
        assert(fabsf(delta) < 0.2f);
        assert(ret == 0);
    }
}
