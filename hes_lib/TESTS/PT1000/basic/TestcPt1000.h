#ifndef _TESTCPT1000_H
#define _TESTCPT1000_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

void Test_ResistanceToTempLUTable_cPt1000(void);

#ifdef __cplusplus
}
#endif
#endif
