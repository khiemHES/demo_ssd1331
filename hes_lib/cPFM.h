#ifndef _CPFM_H
#define _CPFM_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

typedef enum {
    CHANNEL1_cPFM,
    CHANNEL2_cPFM,
    CHANNEL3_cPFM,
    CHANNEL4_cPFM,
} TimerChannel_cPFM;

enum ErrorCode_cPFM {
    NO_ERROR_cPFM,
    PULSE_SET_TIMEOUT_cPFM,
    NUM_ERRORS_cPFM
};

typedef uint32_t (*pGetTick_cPFM)(void);
typedef struct cPFM_t {
    int id;
    uint32_t error_code;
    volatile uint8_t state;
    uint32_t onCounts;    // amount of counts for the pulse length
    uint32_t totalCounts; // the period of cPFM
    uint32_t lastTimeTriggerPulse;
    uint32_t timeout;
    uint32_t totalPulses;
    pGetTick_cPFM GetTick;

    TIM_HandleTypeDef *fastHtimer;
    uint32_t ocChannel;
    uint32_t ocItMask;

    TimerChannel_cPFM cc;

    GPIO_TypeDef *port;
    uint32_t pin;
} cPFM;
void Init_cPFM(cPFM *me);
void Setup_cPFM(cPFM *me, int id, uint32_t onCounts, uint32_t totalCounts,
                uint32_t timeout);
int Update_cPFM(cPFM *me);
void SetCC_cPFM(cPFM *me, TimerChannel_cPFM cc);
int SetTotalCounts_cPFM(cPFM *me, uint32_t totalCounts);
uint32_t GetTotalCounts_cPFM(cPFM *me);
int SetOnCounts_cPFM(cPFM *me, uint32_t onCounts);
uint32_t GetOnCounts_cPFM(cPFM *me);

void SetPulse_cPFM(cPFM *me);
void ResetPulse_cPFM(cPFM *me);

#ifdef __cplusplus
}
#endif
#endif
