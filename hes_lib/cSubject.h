#ifndef _CSUBJECT_H
#define _CSUBJECT_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define MAX_NUMBER_OBSERVER (5)

typedef void (*Notify_Observer_fp)(void *param);

typedef struct cSubject_t {
    int id;
    uint32_t error_code;
    Notify_Observer_fp olist[MAX_NUMBER_OBSERVER];
    int numObserver;
} cSubject;
void Init_cSubject(cSubject *me);
int Setup_cSubject(cSubject *me, Notify_Observer_fp cb);
int Attach_cSubject(cSubject *me, Notify_Observer_fp callback);
int Detach_cSubject(cSubject *me, Notify_Observer_fp callback);
void Notify_cSubject(cSubject *me, void *param);

#ifdef __cplusplus
}
#endif
#endif
