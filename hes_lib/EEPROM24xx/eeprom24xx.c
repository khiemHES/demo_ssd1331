
#include "eeprom24xx.h"
#include "cAssert.h"
#include "cFnCoreUtilities.h"
#include "i2c.h"
#include <assert.h>

#if (_EEPROM_FREERTOS_IS_ENABLE == 1)
#include "cmsis_os2.h"
#endif
//##########################################################################
bool EEPROM24XX_IsConnected(void) {
#if (_EEPROM_USE_WP_PIN == 1)
    HAL_GPIO_WritePin(_EEPROM_WP_GPIO, _EEPROM_WP_PIN, GPIO_PIN_SET);
#endif
    if (HAL_I2C_IsDeviceReady(&_EEPROM24XX_I2C, 0xa0, 1, 100) == HAL_OK)
        return true;
    else
        return false;
}
//##########################################################################
bool EEPROM24XX_Save__(uint16_t Address, const void *data,
                       size_t size_of_data) {
    bool ret = false;

    uint16_t devAddr = 0xA0;
    if (Address >= 0x100) {
        BIT_SET_cFnCoreUtilities(devAddr, 1);
    }

    assert(size_of_data <= 0x10);
    assert(Address % 0x10 == 0 || size_of_data == 1);
    if (HAL_I2C_Mem_Write(&_EEPROM24XX_I2C, devAddr, Address,
                          I2C_MEMADD_SIZE_8BIT, (uint8_t *)data, size_of_data,
                          100) == HAL_OK) {
        ret = true;
        osDelay(7);
    } else {
        ret = false;
    }
    return ret;
}

bool EEPROM24XX_Save(uint16_t Address, const void *data, size_t size_of_data) {
    if (Address + size_of_data > 0x200) {
        assert(0);
    }
    // handle address boudary
    uint16_t wAddr = Address;
    int bLeft = size_of_data;
    void *dPtr = (void *)data;
    size_t wB = 0;
    if (size_of_data == 1) {
        wB = size_of_data;
        return EEPROM24XX_Save__(wAddr, dPtr, wB);
    } else {
        while (bLeft > 0) {
            if (wAddr % 0x10) {
                wB = 1;
            } else {
                wB = 0x10;
            }
            bool ret2 = EEPROM24XX_Save__(wAddr, dPtr, wB);
            if (ret2 == false) {
                return false;
            }
            wAddr += wB;
            dPtr = (void *)(((uint32_t)dPtr) + wB);
            bLeft -= (int)wB;
        }
        return true;
    }
}

//##########################################################################
bool EEPROM24XX_Load(uint16_t Address, const void *data, size_t size_of_data) {
#if (_EEPROM_USE_WP_PIN == 1)
    HAL_GPIO_WritePin(_EEPROM_WP_GPIO, _EEPROM_WP_PIN, GPIO_PIN_SET);
#endif
    if (HAL_I2C_Mem_Read(&_EEPROM24XX_I2C, 0xa0, Address, I2C_MEMADD_SIZE_8BIT,
                         (uint8_t *)data, size_of_data, 100) == HAL_OK) {
        return true;
    } else
        return false;
}
