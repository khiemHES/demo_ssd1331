# I2C EEPROM

## INTRODUCTION

Implement eeprom driver for MICROCHIP 24AA04 4K I2C SERIAL

## DESIGN

Fork from https://github.com/nimaltd/EEPROM24xx

### Interface

### Things to know about i2c hal

- What's the difference between `HAL_I2C_Master_Receive`/`HAL_I2C_Master_Transmit` and `HAL_I2C_Mem_Read`/`HAL_I2C_Mem_Write`?
    - `HAL_I2C_Master_Receive` performs a I2C read operation of N bytes (start, I2C address + Read, N bytes, stop). `HAL_I2C_Master_Transmit` performs a I2C write operation of N bytes (start, I2C address + Write, N bytes, stop).
    - `HAL_I2C_Mem_Read` performs a I2C write operation to select the memory address to read and then reads N bytes (start, I2C address + Write, Memory address, repeated start, I2C address + Read, N bytes, stop) `HAL_I2C_Mem_Write` performs a I2C write operation to select the memory address to read and then writes N bytes (start, I2C address + Write, Memory address, repeated start, I2C address + Write, N bytes, stop)
- Reference
    - https://stackoverflow.com/questions/38230248/how-do-i-use-the-stm32cubef4-hal-library-to-read-out-the-sensor-data-with-i2c
    - https://electronics.stackexchange.com/questions/244491/addressing-registers-with-i2c-stm32f0-hal-libraries

## REFERENCE

  - https://github.com/eleciawhite/STM32Cube/tree/master/STM32Cube_FW_F3_V1.3.0/Projects/STM32373C_EVAL/Examples/I2C/I2C_EEPROM
  - https://github.com/nimaltd/EEPROM24xx
  - https://github.com/macgeorge/STM32-example-codes/tree/master/4%20-%20I2C%20EEPROM%20with%20error%20handling
  - https://ralimtek.com/electronics/stm32-eeprom/
  - http://www.electroons.com/blog/hello-world/
  - https://www.youtube.com/watch?v=qAarZ5tho9g
  - https://www.youtube.com/watch?v=I1meJIccKko
  - http://we.easyelectronics.ru/STM32/stm32-i2c-eeprom-24sxx.html
  - https://forbot.pl/blog/kurs-stm32-11-i2c-w-praktyce-pamiec-eeprom-id10556
  - https://www.christidis.info/index.php/blog/30-i2c-eeprom-with-error-handling-on-an-stm32-microcontroller
  - https://eax.me/stm32-external-eeprom/
  - https://github.com/sinadarvi/SD_HAL_AT24
  - https://elixir.bootlin.com/linux/latest/source/drivers/misc/eeprom/at24.c
  - https://github.com/PaoloP74/extEEPROM
  - https://github.com/maniacbug/M24LC256
