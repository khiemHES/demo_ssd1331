/**
 * @brief cADS1118 represent the hardware of ads1118. it provides interfaces to
 * control ads1118.
 *
 * ##IMPORTANT
 *      even set config in SINGLE_SHOT_ADS1118 + SINGLE_CONVER_START_ADS1118,
 * ADS1118 only does conversion according to data setting, NOT at the time that
 * it receives SINGLE_CONVER cmd. Thus if set DR_860_SPS_ADS1118, the waiting
 * for conversion finish will be shorter. Depend on application, the following
 * parameters need to be defined accordingly
 *      DATA_RATE_HES_ADS1118
 *
 * ##HOW TO USE
 * - Setup
 *   - Init_cADS1118
 *   - Setup_cADS1118
 *   - SaveConfigReg_cADS1118
 *   - UpdateConfigReg_cADS1118
 *   - IsConfigUpdated_cADS1118
 * - Loop
 *   - get ic_temp
 *     - ChangeADCMux_cADS1118
 *     - UpdateConfigReg_cADS1118
 *     - GetADCVal_cADS1118
 *   - get AINPN_0_1_ADS1118
 *     - ChangeADCMux_cADS1118
 *     - UpdateConfigReg_cADS1118
 *     - GetADCVal_cADS1118
 *
 * @file cADS1118.c
 * @author DO HOANG DUY KHIEM
 * @date 2018-03-07
 */

#include "cADS1118.h"
#include "cFnCoreUtilities.h"
#include <assert.h>
#include <math.h>
#include <string.h>

void WriteNSSPin_cADS118(cADS1118 *me, int flag) { //
    if (me->nssPort == 0 || me->nssPin == 0)
        return;
    if (flag) {
        HAL_GPIO_WritePin(me->nssPort, me->nssPin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(me->nssPort, me->nssPin, GPIO_PIN_RESET);
    }
}
int GetNSSPinState_cADS118(cADS1118 *me, int flag) { //
    return HAL_GPIO_ReadPin(me->nssPort, me->nssPin);
}
/**
 * @brief WriteSPI is written specifically for ADS1118
 *  For this ic, tx msg is either dummy data or MUST be configReg val.
 *  if numBytes = 0 --> send dummy data
 *  if == 2 --> send configReg 1 time
 *  if == 4 --> send configReg 2 times
 *  if transmission s failed, it will set error_code
 * @param me
 * @param numBytes number of bytes to transmit on SPI
 * @return int
 *      =0 --> okie
 *      =1 --> get error
 */
int WriteSPI_cADS118(cADS1118 *me) {
    int ret = 0;
    int numBytes = 0;

    // prepare txSpiBuffer
    memset(me->txSpiBuffer, 0, sizeof(me->txSpiBuffer));
    memset(me->rxSpiBuffer, 0, sizeof(me->txSpiBuffer));
    uint8_t *pTxSPI = (me->txSpiBuffer);
    uint16_t configBigEndian =
        SWAP_UINT16T_cFnCoreUtilities(me->configReg.word);

    switch (me->transmissionType) {
    case NO_CONFIGURE_cADS1118:
        numBytes = 2;
        break;
    case TRANSMISSION_16B_cADS1118:
        numBytes = 2;
        memcpy(pTxSPI, &configBigEndian, 2);
        break;
    case TRANSMISSION_32B_cADS1118:
        numBytes = 4;
        memcpy(pTxSPI, &configBigEndian, 2);
        memcpy(pTxSPI + 2, &configBigEndian, 2);
        break;
    }

    // spiSend
    if (osSemaphoreGetCount(me->cpltFlag) != 0) {
        osSemaphoreAcquire(me->cpltFlag, 0);
        me->timeAtTransmit = osKernelGetTickCount();
        WriteNSSPin_cADS118(me, 0);
        int ret1 = HAL_SPI_TransmitReceive_DMA(me->hspi, me->txSpiBuffer,
                                               me->rxSpiBuffer, numBytes);
        // error handle
        if (ret1 != HAL_OK) {
            me->error_code = HAL_SPI_ERROR_cADS1118;
            ret = 1;
        }
    } else {
        me->error_code = SEMAPHORE_ERROR_cADS1118;
        ret = 1;
    }
    return ret;
}
int IsComCptl_cADS1118(cADS1118 *me) {
    return (int)(osSemaphoreGetCount(me->cpltFlag) > 0);
}
/**
 * @brief check the NOP flag of received configReg val
 *
 * @param me
 * @return int
 *      =1 yes updated
 */
int IsConfigUpdated_cADS1118(cADS1118 *me) {
    int ret = 0;
    if (IsComCptl_cADS1118(me)) {
        ADS1118_Config_Reg config;
        uint16_t val = *((uint16_t *)&me->rxSpiBuffer[2]);
        config.word = SWAP_UINT16T_cFnCoreUtilities(val);
        ret = (config.fields.NOP == 1);
    }
    return ret;
}
int GetRxConfig_cADS1118(cADS1118 *me, ADS1118_Config_Reg *config) {
    int ret = 1;
    if (osSemaphoreGetCount(me->cpltFlag) != 0) {
        uint16_t val = *((uint16_t *)&me->rxSpiBuffer[2]);
        config->word = SWAP_UINT16T_cFnCoreUtilities(val);
        ret = 0;
    }
    return ret;
}
int GetICTemp_cADS1118(cADS1118 *me, float *icTemp,
                       ADS1118_Config_Reg *config) {
    int ret = 1;
    if (osSemaphoreGetCount(me->cpltFlag) != 0 &&
        config->fields.TS_MODE == TEMPERATURE_MODE_ADS1118) {
        uint16_t adcVal = *((uint16_t *)&me->rxSpiBuffer[0]);
        adcVal = SWAP_UINT16T_cFnCoreUtilities(adcVal) >> 2;
        *icTemp = ((float)((int16_t)adcVal) * ADS1118_CONST_1_024V_LSB_mV);
        ret = 0;
    }
    return ret;
}
int GetVoltageVal_cADS1118(cADS1118 *me, float *voltageVal,
                           ADS1118_Config_Reg *config) {
    int ret = 1;
    if (osSemaphoreGetCount(me->cpltFlag) != 0 &&
        config->fields.TS_MODE == ADC_MODE_ADS1118) {
        uint16_t adcVal = *((uint16_t *)&me->rxSpiBuffer[0]);
        adcVal = SWAP_UINT16T_cFnCoreUtilities(adcVal);
        if (adcVal != 0x8000 && adcVal != 0x7FFF) {
            float scale = 0;
            switch (config->fields.PGA) {
            case PGA_6144_ADS1118:
                scale = ADS1118_CONST_6_144V_LSB_mV;
                break;
            case PGA_4096_ADS1118:
                scale = ADS1118_CONST_4_096V_LSB_mV;
                break;
            case PGA_2048_ADS1118:
                scale = ADS1118_CONST_2_048V_LSB_mV;
                break;
            case PGA_1024_ADS1118:
                scale = ADS1118_CONST_1_024V_LSB_mV;
                break;
            case PGA_512_ADS1118:
                scale = ADS1118_CONST_0_512V_LSB_mV;
                break;
            case PGA_256_ADS1118:
                scale = ADS1118_CONST_0_256V_LSB_mV;
                break;
            }
            *voltageVal = ((float)((int16_t)adcVal)) * scale;
            ret = 0;
        } else {
            if (adcVal == 0x8000) {
                me->error_code = ADC_BEYOND_LOWER_BOUND_cADS1118;
            } else { // 0x7fff
                me->error_code = ADC_BEYOND_UPER_BOUND_cADS1118;
            }
            ret = 1;
        }
    }
    return ret;
}
ADS_MUX_TypeDef GetCurrentADCMuxConfig_cADS1118(cADS1118 *me) {
    ADS_MUX_TypeDef ret = NO_MUX_ADS1118;
    if (me->configReg.fields.TS_MODE == 1) {
        ret = INTERNAL_IC_TEMPERATURE_ADS1118;
    } else {
        ret = (ADS_MUX_TypeDef)me->configReg.fields.MUX;
    }
    return ret;
}
/**
 * @brief just save the config into the internal memory. Only after calling
 * UpdateConfigReg_cADS1118, it can make sure that the config is updated
 *
 * @param me
 * @param config
 * @return int
 */
int SaveConfigReg_cADS1118(cADS1118 *me, ADS1118_Config_Reg *config) {
    return (int)memcpy(&me->configReg, config, sizeof(ADS1118_Config_Reg));
}
/**
 * @brief change TS_Mode or Mux while keeping other saved configures
 *
 * @param me
 * @param mux
 */
void ChangeADCMux_cADS1118(cADS1118 *me, ADS_MUX_TypeDef mux) {
    if (mux == INTERNAL_IC_TEMPERATURE_ADS1118) {
        me->configReg.fields.TS_MODE = TEMPERATURE_MODE_ADS1118;
    } else {
        me->configReg.fields.MUX = mux;
        me->configReg.fields.TS_MODE = ADC_MODE_ADS1118;
    }
}
void HandleTxRxCpltCallback_cADS1118(cADS1118 *me) {
    int ret = osSemaphoreRelease(me->cpltFlag);
    WriteNSSPin_cADS118(me, 1);
}
void Init_cADS1118(cADS1118 *me) { //
    memset(me, 0, sizeof(cADS1118));
}
int Setup_cADS1118(cADS1118 *me) {
    me->cpltFlag = osSemaphoreNew(1, 1, NULL);
    return 0;
}
