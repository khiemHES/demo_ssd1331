#ifndef _CSWALARM_H
#define _CSWALARM_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

typedef enum TypeID_cSWAlarm_t {
    ONETIME_TypeID_cSWAlarm,
    PERIODIC_TypeID_cSWAlarm,
    NUM_TypeID_cSWAlarm
} TypeID_cSWAlarm;

typedef uint32_t (*func_cSWAlarm)(void *param);

typedef struct cSWAlarm_t {
    uint32_t Mixed_AlarmTime_and_RunState__;
    TypeID_cSWAlarm type;
    uint32_t lastTime;
    uint32_t duration;
    uint32_t currentTime;
    func_cSWAlarm onAlarm;
    func_cSWAlarm getTick;
    void *usrParam;
} cSWAlarm;

void Init_cSWAlarm(cSWAlarm *me);
int Setup_cSWAlarm(cSWAlarm *me, uint32_t duration, func_cSWAlarm getTick,
                   func_cSWAlarm onAlarm);
int UpdateNCheck_cSWAlarm(cSWAlarm *me);
int IsRunning_cSWAlarm(cSWAlarm *me);
void Stop_cSWAlarm(cSWAlarm *me);
int Start_cSWAlarm(cSWAlarm *me);
void Reset_cSWAlarm(cSWAlarm *me);
void SetDuration_cSWAlarm(cSWAlarm *me, uint32_t duration);
uint32_t GetDuration_cSWAlarm(cSWAlarm *me);
#ifdef __cplusplus
}
#endif
#endif
