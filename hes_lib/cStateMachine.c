#include "cStateMachine.h"
#include "string.h"

cStateMachine_Event reservedEvt_[] = {
    {(uint32_t)cStateMachine_NULL_SIG, 0, 0},
    {(uint32_t)cStateMachine_ENTRY_SIG, 0, 0},
    {(uint32_t)cStateMachine_EXIT_SIG, 0, 0},
    {(uint32_t)cStateMachine_INIT_SIG, 0, 0},
    {(uint32_t)cStateMachine_USER_SIG, 0, 0} //
};

int cStateMachineF_Init(cStateMachine *me) {
    (void)memset(me, 0, sizeof(cStateMachine));
    return 0;
}
void cStateMachineF_Initialize(cStateMachine *me, cStateMachine_Event *e) {
    if (me->curr_handler) {
        (void)(*me->curr_handler)(
            me, e); // initial with signal _EMPTY_SIG_--> execute FSMInitilize
    }
    if (me->curr_handler) {
        (void)(*me->curr_handler)(
            me,
            &reservedEvt_[cStateMachine_ENTRY_SIG]); // exe with signal
                                                     // _ENTRY_SIG--> execute
                                                     // the entry routine of the
                                                     // first state
    }
}
void cStateMachineF_Handler(cStateMachine *me, cStateMachine_Event *e) {
    cStateMachine_StateAction ret = cStateMachine_HOLD_STATE;

    if (me->curr_handler) {
        me->last_handler_ = me->curr_handler;
        ret = (*me->last_handler_)(me, e);

        if (ret == cStateMachine_CHANGE_STATE) {
            (void)(*me->last_handler_)(me,
                                       &reservedEvt_[cStateMachine_EXIT_SIG]);
            if (me->curr_handler) {
                (void)(*me->curr_handler)(
                    me, &reservedEvt_[cStateMachine_ENTRY_SIG]);
            }
        }
    }
}
cStateMachine_StateAction
cStateMachineF_SetState(cStateMachine *me, int newState,
                        cStateMachine_StateHandler newHandler) {
    if (me->curr_state != newState) {
        me->last_state_ = me->curr_state;
        me->curr_state = newState;
        me->curr_handler = newHandler;
        return cStateMachine_CHANGE_STATE;
    } else {
        return cStateMachine_HOLD_STATE;
    }
}
int cStateMachineF_GetLastState(cStateMachine *me) { return me->last_state_; }
