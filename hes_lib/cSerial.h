#ifndef _CSERIAL_H
#define _CSERIAL_H
#ifdef __cplusplus
extern "C" {
#endif

#include "cRingBuffer.h"
#include "main.h"

#define DEFAULT_BLOCK_TIME_cSerial (100)
#define INTERRUPT_SERIAL_FEATURE_cSerial

enum ErrorCode_cSerial {
    NO_ERROR_cSerial,
    QUEUE_NO_SPACE_cSerial,
    QUEUE_OTHER_ERROR_cSerial,
    SEMAPHORE_ERROR_cSerial,
    MUTEX_PUTTING_MSG_cSerial,
    HAL_TRANSMIT_ERROR_cSerial,
    NUM_ERRORS_cSerial
};

typedef struct cSerial_t {
    int id;
    uint32_t error_code;
    uint32_t rxBlockTime;
    uint32_t txBlockTime;
    UART_HandleTypeDef *driver;
    char txChar;
    char rxChar;
    osSemaphoreId_t txCpltFlag;
    osMessageQueueId_t rxQueue;
    osMessageQueueId_t txQueue;
    cRingBuffer txRBuff;
} cSerial;

void Init_cSerial(struct cSerial_t *me);
int Setup_cSerial(cSerial *me, UART_HandleTypeDef *huart,
                     size_t txMgCount, size_t rxMgCount);
int GetChar_cSerial(struct cSerial_t *me, uint8_t *rxChar_p);
void HandleRxCpltCallback_cSerial(struct cSerial_t *me);
int PutChar_cSerial(struct cSerial_t *me, uint8_t txChar);
int PutString_cSerial(struct cSerial_t *me, uint8_t *txStr);
void Update_cSerial(struct cSerial_t *me);
void HandleTxCpltCallback_cSerial(struct cSerial_t *me);

#ifdef __cplusplus
}
#endif
#endif
