#ifndef 	_CRINGBUFFER_H
#define 	_CRINGBUFFER_H

#include "main.h"
#include <string.h>
#include <stdlib.h>

typedef struct RingBuffer {
	int8_t *buffer;
	volatile int32_t head, tail, active;
	int32_t itemSize, bufferSize;
	int32_t circularEnable;
} cRingBuffer;

void Init_cRingBuffer(cRingBuffer* me);
void Setup_cRingBuffer(cRingBuffer* me, uint32_t numberItems, uint32_t sizeItem);
void Push_cRingBuffer(cRingBuffer* me, void* item);
int32_t Pop_cRingBuffer(cRingBuffer* me, void *item);
int32_t GetNumberItemLeft_cRingBuffer(cRingBuffer* me);
int32_t GetNumberItemUsed_cRingBuffer(cRingBuffer* me);
int32_t GetNumberByteLeft_cRingBuffer(cRingBuffer* me);
int32_t GetNumberByteUsed_cRingBuffer(cRingBuffer* me);
void * PeekTailItemPtr_cRingBuffer(cRingBuffer* me);
void * PeekNextItemPtr_cRingBuffer(cRingBuffer* me, void* currentPtr);
void * PeekHeadItemPtr_cRingBuffer(cRingBuffer* me);

void Flush_cRingBuffer(cRingBuffer* me);
#endif
